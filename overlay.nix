self: super: {
  haskellPackages = super.haskellPackages.override (oldArgs: {
    overrides =
      self.lib.composeExtensions (oldArgs.overrides or (_: _: {}))
        (hself: hsuper: {
          c2hsc = self.haskell.lib.dontCheck hsuper.c2hsc;
        });
  });
}

