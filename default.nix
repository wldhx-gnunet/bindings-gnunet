{ pkgs ?
  import (fetchTarball "https://nixos.org/channels/nixos-18.09-small/nixexprs.tar.xz") {
    overlays = [ (import ./overlay.nix) ];
  }
}:
pkgs.haskellPackages.callPackage ./bindings-gnunet.nix { }
