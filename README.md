# bindings-gnunet

## Development

### Incremental gnunet builds

If you'd like to hack on both bindings and gnunet upstream at the same time, getting gnunet via nix -- i.e. rebuilding it on every source change -- will be inconvenient. For incremental builds, remove gnunet from `bindings-gnunet.nix`, and patch `bindings-gnunet.cabal` like so:

```
-  pkgconfig-depends:   gnunetutil
+  extra-libraries  :   gnunetutil
                      , gnunetats
                      , gnunetstatistics
                      , gnunettransport
                      , gnunethello
                      , gnunetcadet
+  include-dirs:        /my/local/gnunet/dist/include
+  extra-lib-dirs:      /my/local/gnunet/dist/lib
```

Assuming you're building gnunet to `/my/local/gnunet/dist`.
