{ mkDerivation, base, bindings-DSL, c2hs, c2hsc, gnunet, hspec
, hspec-discover, stdenv
}:
mkDerivation {
  pname = "bindings-gnunet";
  version = "0.0.0.1";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [ base bindings-DSL ];
  libraryPkgconfigDepends = [ gnunet ];
  libraryToolDepends = [ c2hs c2hsc ];
  executableHaskellDepends = [ base ];
  testHaskellDepends = [ base hspec ];
  testToolDepends = [ hspec-discover ];
  license = stdenv.lib.licenses.agpl3;
}
