{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_configuration_lib.h"
module Bindings.GNUnet.GnunetConfigurationLib where
import Foreign.Ptr
#strict_import

{- struct GNUNET_CONFIGURATION_Handle; -}
#opaque_t struct GNUNET_CONFIGURATION_Handle
#ccall GNUNET_CONFIGURATION_create , IO (Ptr <struct GNUNET_CONFIGURATION_Handle>)
#ccall GNUNET_CONFIGURATION_dup , Ptr <struct GNUNET_CONFIGURATION_Handle> -> IO (Ptr <struct GNUNET_CONFIGURATION_Handle>)
#ccall GNUNET_CONFIGURATION_destroy , Ptr <struct GNUNET_CONFIGURATION_Handle> -> IO ()
#ccall GNUNET_CONFIGURATION_load , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> IO CInt
#ccall GNUNET_CONFIGURATION_load_from , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> IO CInt
#ccall GNUNET_CONFIGURATION_parse , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> IO CInt
#ccall GNUNET_CONFIGURATION_serialize , Ptr <struct GNUNET_CONFIGURATION_Handle> -> Ptr CSize -> IO CString
#ccall GNUNET_CONFIGURATION_deserialize , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> CSize -> CString -> IO CInt
#ccall GNUNET_CONFIGURATION_write , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> IO CInt
#ccall GNUNET_CONFIGURATION_write_diffs , Ptr <struct GNUNET_CONFIGURATION_Handle> -> Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> IO CInt
#ccall GNUNET_CONFIGURATION_get_diff , Ptr <struct GNUNET_CONFIGURATION_Handle> -> Ptr <struct GNUNET_CONFIGURATION_Handle> -> IO (Ptr <struct GNUNET_CONFIGURATION_Handle>)
#ccall GNUNET_CONFIGURATION_is_dirty , Ptr <struct GNUNET_CONFIGURATION_Handle> -> IO CInt
#callback GNUNET_CONFIGURATION_Iterator , Ptr () -> CString -> CString -> CString -> IO ()
#callback GNUNET_CONFIGURATION_Section_Iterator , Ptr () -> CString -> IO ()
#ccall GNUNET_CONFIGURATION_iterate , Ptr <struct GNUNET_CONFIGURATION_Handle> -> <GNUNET_CONFIGURATION_Iterator> -> Ptr () -> IO ()
#ccall GNUNET_CONFIGURATION_iterate_sections , Ptr <struct GNUNET_CONFIGURATION_Handle> -> <GNUNET_CONFIGURATION_Section_Iterator> -> Ptr () -> IO ()
#ccall GNUNET_CONFIGURATION_remove_section , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> IO ()
#ccall GNUNET_CONFIGURATION_get_value_number , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> CString -> Ptr CULong -> IO CInt
#ccall GNUNET_CONFIGURATION_get_value_float , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> CString -> Ptr CFloat -> IO CInt
#ccall GNUNET_CONFIGURATION_get_value_size , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> CString -> Ptr CULong -> IO CInt
#ccall GNUNET_CONFIGURATION_have_value , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> CString -> IO CInt
#ccall GNUNET_CONFIGURATION_get_value_string , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> CString -> Ptr CString -> IO CInt
#ccall GNUNET_CONFIGURATION_get_value_filename , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> CString -> Ptr CString -> IO CInt
#ccall GNUNET_CONFIGURATION_iterate_section_values , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> <GNUNET_CONFIGURATION_Iterator> -> Ptr () -> IO ()
#ccall GNUNET_CONFIGURATION_get_value_choice , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> CString -> Ptr CString -> Ptr CString -> IO CInt
#ccall GNUNET_CONFIGURATION_get_value_yesno , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> CString -> IO CInt
#ccall GNUNET_CONFIGURATION_get_data , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> CString -> Ptr () -> CSize -> IO CInt
#ccall GNUNET_CONFIGURATION_expand_dollar , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> IO CString
#ccall GNUNET_CONFIGURATION_set_value_number , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> CString -> CULong -> IO ()
#ccall GNUNET_CONFIGURATION_set_value_string , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> CString -> CString -> IO ()
#ccall GNUNET_CONFIGURATION_remove_value_filename , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> CString -> CString -> IO CInt
#ccall GNUNET_CONFIGURATION_append_value_filename , Ptr <struct GNUNET_CONFIGURATION_Handle> -> CString -> CString -> CString -> IO CInt
