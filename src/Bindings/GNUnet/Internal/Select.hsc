{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "sys/select.h"
module Bindings.GNUnet.Internal.Select where
import Foreign.Ptr
#strict_import

{- typedef __suseconds_t suseconds_t; -}
#synonym_t suseconds_t , CLong
{- typedef long int __fd_mask; -}
#synonym_t __fd_mask , CLong
{- typedef struct {
            __fd_mask __fds_bits[1024 / (8 * (int) sizeof(__fd_mask))];
        } fd_set; -}
#starttype fd_set
#array_field __fds_bits , CLong
#stoptype
{- typedef __fd_mask fd_mask; -}
#synonym_t fd_mask , CLong
