{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_disk_lib.h"
module Bindings.GNUnet.GnunetDiskLib where
import Foreign.Ptr
#strict_import

import Bindings.GNUnet.GnunetCommon
{- struct GNUNET_DISK_PipeHandle; -}
#opaque_t struct GNUNET_DISK_PipeHandle
{- enum GNUNET_FILE_Type {
    GNUNET_DISK_HANLDE_TYPE_EVENT,
    GNUNET_DISK_HANLDE_TYPE_FILE,
    GNUNET_DISK_HANLDE_TYPE_PIPE
}; -}
#integral_t enum GNUNET_FILE_Type
#num GNUNET_DISK_HANLDE_TYPE_EVENT
#num GNUNET_DISK_HANLDE_TYPE_FILE
#num GNUNET_DISK_HANLDE_TYPE_PIPE
{- struct GNUNET_DISK_FileHandle {
    int fd;
}; -}
#starttype struct GNUNET_DISK_FileHandle
#field fd , CInt
#stoptype
{- enum GNUNET_DISK_OpenFlags {
    GNUNET_DISK_OPEN_READ = 1,
    GNUNET_DISK_OPEN_WRITE = 2,
    GNUNET_DISK_OPEN_READWRITE = 3,
    GNUNET_DISK_OPEN_FAILIFEXISTS = 4,
    GNUNET_DISK_OPEN_TRUNCATE = 8,
    GNUNET_DISK_OPEN_CREATE = 16,
    GNUNET_DISK_OPEN_APPEND = 32
}; -}
#integral_t enum GNUNET_DISK_OpenFlags
#num GNUNET_DISK_OPEN_READ
#num GNUNET_DISK_OPEN_WRITE
#num GNUNET_DISK_OPEN_READWRITE
#num GNUNET_DISK_OPEN_FAILIFEXISTS
#num GNUNET_DISK_OPEN_TRUNCATE
#num GNUNET_DISK_OPEN_CREATE
#num GNUNET_DISK_OPEN_APPEND
{- enum GNUNET_DISK_MapType {
    GNUNET_DISK_MAP_TYPE_READ = 1,
    GNUNET_DISK_MAP_TYPE_WRITE = 2,
    GNUNET_DISK_MAP_TYPE_READWRITE = 3
}; -}
#integral_t enum GNUNET_DISK_MapType
#num GNUNET_DISK_MAP_TYPE_READ
#num GNUNET_DISK_MAP_TYPE_WRITE
#num GNUNET_DISK_MAP_TYPE_READWRITE
{- enum GNUNET_DISK_AccessPermissions {
    GNUNET_DISK_PERM_NONE = 0,
    GNUNET_DISK_PERM_USER_READ = 1,
    GNUNET_DISK_PERM_USER_WRITE = 2,
    GNUNET_DISK_PERM_USER_EXEC = 4,
    GNUNET_DISK_PERM_GROUP_READ = 8,
    GNUNET_DISK_PERM_GROUP_WRITE = 16,
    GNUNET_DISK_PERM_GROUP_EXEC = 32,
    GNUNET_DISK_PERM_OTHER_READ = 64,
    GNUNET_DISK_PERM_OTHER_WRITE = 128,
    GNUNET_DISK_PERM_OTHER_EXEC = 256
}; -}
#integral_t enum GNUNET_DISK_AccessPermissions
#num GNUNET_DISK_PERM_NONE
#num GNUNET_DISK_PERM_USER_READ
#num GNUNET_DISK_PERM_USER_WRITE
#num GNUNET_DISK_PERM_USER_EXEC
#num GNUNET_DISK_PERM_GROUP_READ
#num GNUNET_DISK_PERM_GROUP_WRITE
#num GNUNET_DISK_PERM_GROUP_EXEC
#num GNUNET_DISK_PERM_OTHER_READ
#num GNUNET_DISK_PERM_OTHER_WRITE
#num GNUNET_DISK_PERM_OTHER_EXEC
{- enum GNUNET_DISK_Seek {
    GNUNET_DISK_SEEK_SET = 0,
    GNUNET_DISK_SEEK_CUR = 1,
    GNUNET_DISK_SEEK_END = 2
}; -}
#integral_t enum GNUNET_DISK_Seek
#num GNUNET_DISK_SEEK_SET
#num GNUNET_DISK_SEEK_CUR
#num GNUNET_DISK_SEEK_END
{- enum GNUNET_DISK_PipeEnd {
    GNUNET_DISK_PIPE_END_READ = 0, GNUNET_DISK_PIPE_END_WRITE = 1
}; -}
#integral_t enum GNUNET_DISK_PipeEnd
#num GNUNET_DISK_PIPE_END_READ
#num GNUNET_DISK_PIPE_END_WRITE
#ccall GNUNET_DISK_handle_invalid , Ptr <struct GNUNET_DISK_FileHandle> -> IO CInt
#ccall GNUNET_DISK_file_test , CString -> IO CInt
#ccall GNUNET_DISK_file_backup , CString -> IO ()
#ccall GNUNET_DISK_file_seek , Ptr <struct GNUNET_DISK_FileHandle> -> CLong -> <enum GNUNET_DISK_Seek> -> IO CLong
#ccall GNUNET_DISK_file_size , CString -> Ptr CULong -> CInt -> CInt -> IO CInt
#ccall GNUNET_DISK_file_get_identifiers , CString -> Ptr CULong -> Ptr CULong -> IO CInt
#ccall GNUNET_DISK_mktemp , CString -> IO CString
#ccall GNUNET_DISK_mkdtemp , CString -> IO CString
#ccall GNUNET_DISK_file_open , CString -> <enum GNUNET_DISK_OpenFlags> -> <enum GNUNET_DISK_AccessPermissions> -> IO (Ptr <struct GNUNET_DISK_FileHandle>)
#ccall GNUNET_DISK_file_handle_size , Ptr <struct GNUNET_DISK_FileHandle> -> Ptr CLong -> IO CInt
#ccall GNUNET_DISK_pipe , CInt -> CInt -> CInt -> CInt -> IO (Ptr <struct GNUNET_DISK_PipeHandle>)
#ccall GNUNET_DISK_pipe_from_fd , CInt -> CInt -> Ptr CInt -> IO (Ptr <struct GNUNET_DISK_PipeHandle>)
#ccall GNUNET_DISK_pipe_close , Ptr <struct GNUNET_DISK_PipeHandle> -> IO CInt
#ccall GNUNET_DISK_pipe_close_end , Ptr <struct GNUNET_DISK_PipeHandle> -> <enum GNUNET_DISK_PipeEnd> -> IO CInt
#ccall GNUNET_DISK_pipe_detach_end , Ptr <struct GNUNET_DISK_PipeHandle> -> <enum GNUNET_DISK_PipeEnd> -> IO (Ptr <struct GNUNET_DISK_FileHandle>)
#ccall GNUNET_DISK_file_close , Ptr <struct GNUNET_DISK_FileHandle> -> IO CInt
#ccall GNUNET_DISK_pipe_handle , Ptr <struct GNUNET_DISK_PipeHandle> -> <enum GNUNET_DISK_PipeEnd> -> IO (Ptr <struct GNUNET_DISK_FileHandle>)
#ccall GNUNET_DISK_fix_permissions , CString -> CInt -> CInt -> IO ()
#ccall GNUNET_DISK_get_handle_from_int_fd , CInt -> IO (Ptr <struct GNUNET_DISK_FileHandle>)
#ccall GNUNET_DISK_file_read , Ptr <struct GNUNET_DISK_FileHandle> -> Ptr () -> CSize -> IO CLong
#ccall GNUNET_DISK_file_read_non_blocking , Ptr <struct GNUNET_DISK_FileHandle> -> Ptr () -> CSize -> IO CLong
#ccall GNUNET_DISK_fn_read , CString -> Ptr () -> CSize -> IO CLong
#ccall GNUNET_DISK_file_write , Ptr <struct GNUNET_DISK_FileHandle> -> Ptr () -> CSize -> IO CLong
#ccall GNUNET_DISK_file_write_blocking , Ptr <struct GNUNET_DISK_FileHandle> -> Ptr () -> CSize -> IO CLong
#ccall GNUNET_DISK_fn_write , CString -> Ptr () -> CSize -> <enum GNUNET_DISK_AccessPermissions> -> IO CLong
#ccall GNUNET_DISK_file_copy , CString -> CString -> IO CInt
#ccall GNUNET_DISK_directory_scan , CString -> <GNUNET_FileNameCallback> -> Ptr () -> IO CInt
#ccall GNUNET_DISK_directory_create_for_file , CString -> IO CInt
#ccall GNUNET_DISK_directory_test , CString -> CInt -> IO CInt
#ccall GNUNET_DISK_directory_remove , CString -> IO CInt
#ccall GNUNET_DISK_purge_cfg_dir , CString -> CString -> IO ()
#ccall GNUNET_DISK_directory_create , CString -> IO CInt
#ccall GNUNET_DISK_file_lock , Ptr <struct GNUNET_DISK_FileHandle> -> CLong -> CLong -> CInt -> IO CInt
#ccall GNUNET_DISK_file_unlock , Ptr <struct GNUNET_DISK_FileHandle> -> CLong -> CLong -> IO CInt
#ccall GNUNET_DISK_filename_canonicalize , CString -> IO ()
#ccall GNUNET_DISK_file_change_owner , CString -> CString -> IO CInt
{- struct GNUNET_DISK_MapHandle; -}
#opaque_t struct GNUNET_DISK_MapHandle
#ccall GNUNET_DISK_file_map , Ptr <struct GNUNET_DISK_FileHandle> -> Ptr (Ptr <struct GNUNET_DISK_MapHandle>) -> <enum GNUNET_DISK_MapType> -> CSize -> IO (Ptr ())
#ccall GNUNET_DISK_file_unmap , Ptr <struct GNUNET_DISK_MapHandle> -> IO CInt
#ccall GNUNET_DISK_file_sync , Ptr <struct GNUNET_DISK_FileHandle> -> IO CInt
