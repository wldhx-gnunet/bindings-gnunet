{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_ats_service.h"
module Bindings.GNUnet.GnunetAtsService where
import Foreign.Ptr
#strict_import

import Bindings.GNUnet.GnunetCommon
import Bindings.GNUnet.GnunetConstants
import Bindings.GNUnet.GnunetConfigurationLib (C'GNUNET_CONFIGURATION_Handle)
import Bindings.GNUnet.GnunetCryptoTypes
import Bindings.GNUnet.GnunetTimeLib (C'GNUNET_TIME_Absolute, C'GNUNET_TIME_Relative, C'GNUNET_TIME_RelativeNBO)
import Bindings.GNUnet.GnunetHelloTypes
import Bindings.GNUnet.GnunetNtLib

{- struct GNUNET_ATS_Properties {
    struct GNUNET_TIME_Relative delay;
    uint32_t utilization_out;
    uint32_t utilization_in;
    unsigned int distance;
    enum GNUNET_NetworkType scope;
}; -}
#starttype struct GNUNET_ATS_Properties
#field delay , <struct GNUNET_TIME_Relative>
#field utilization_out , CUInt
#field utilization_in , CUInt
#field distance , CUInt
#field scope , <enum GNUNET_NetworkType>
#stoptype
{- struct GNUNET_ATS_PropertiesNBO {
    uint32_t utilization_out __attribute__((packed));
    uint32_t utilization_in __attribute__((packed));
    uint32_t scope __attribute__((packed));
    uint32_t distance __attribute__((packed));
    struct GNUNET_TIME_RelativeNBO delay;
}; -}
#starttype struct GNUNET_ATS_PropertiesNBO
#field utilization_out , CUInt
#field utilization_in , CUInt
#field scope , CUInt
#field distance , CUInt
#field delay , <struct GNUNET_TIME_RelativeNBO>
#stoptype
#ccall GNUNET_ATS_properties_hton , Ptr <struct GNUNET_ATS_PropertiesNBO> -> Ptr <struct GNUNET_ATS_Properties> -> IO ()
#ccall GNUNET_ATS_properties_ntoh , Ptr <struct GNUNET_ATS_Properties> -> Ptr <struct GNUNET_ATS_PropertiesNBO> -> IO ()
{- struct GNUNET_ATS_ConnectivityHandle; -}
#opaque_t struct GNUNET_ATS_ConnectivityHandle
{- struct GNUNET_ATS_ConnectivitySuggestHandle; -}
#opaque_t struct GNUNET_ATS_ConnectivitySuggestHandle
#ccall GNUNET_ATS_connectivity_init , Ptr <struct GNUNET_CONFIGURATION_Handle> -> IO (Ptr <struct GNUNET_ATS_ConnectivityHandle>)
#ccall GNUNET_ATS_connectivity_done , Ptr <struct GNUNET_ATS_ConnectivityHandle> -> IO ()
#ccall GNUNET_ATS_connectivity_suggest , Ptr <struct GNUNET_ATS_ConnectivityHandle> -> Ptr <struct GNUNET_PeerIdentity> -> CUInt -> IO (Ptr <struct GNUNET_ATS_ConnectivitySuggestHandle>)
#ccall GNUNET_ATS_connectivity_suggest_cancel , Ptr <struct GNUNET_ATS_ConnectivitySuggestHandle> -> IO ()
{- struct GNUNET_ATS_SchedulingHandle; -}
#opaque_t struct GNUNET_ATS_SchedulingHandle
{- struct GNUNET_ATS_Session; -}
#opaque_t struct GNUNET_ATS_Session
#ccall GNUNET_ATS_scheduling_done , Ptr <struct GNUNET_ATS_SchedulingHandle> -> IO ()
{- struct GNUNET_ATS_AddressRecord; -}
#opaque_t struct GNUNET_ATS_AddressRecord
#ccall GNUNET_ATS_address_add , Ptr <struct GNUNET_ATS_SchedulingHandle> -> Ptr <struct GNUNET_HELLO_Address> -> Ptr <struct GNUNET_ATS_Session> -> Ptr <struct GNUNET_ATS_Properties> -> IO (Ptr <struct GNUNET_ATS_AddressRecord>)
#ccall GNUNET_ATS_address_add_session , Ptr <struct GNUNET_ATS_AddressRecord> -> Ptr <struct GNUNET_ATS_Session> -> IO ()
#ccall GNUNET_ATS_address_del_session , Ptr <struct GNUNET_ATS_AddressRecord> -> Ptr <struct GNUNET_ATS_Session> -> IO CInt
#ccall GNUNET_ATS_address_update , Ptr <struct GNUNET_ATS_AddressRecord> -> Ptr <struct GNUNET_ATS_Properties> -> IO ()
#ccall GNUNET_ATS_address_destroy , Ptr <struct GNUNET_ATS_AddressRecord> -> IO ()
{- struct GNUNET_ATS_PerformanceHandle; -}
#opaque_t struct GNUNET_ATS_PerformanceHandle
{- struct GNUNET_ATS_AddressListHandle; -}
#opaque_t struct GNUNET_ATS_AddressListHandle
#ccall GNUNET_ATS_performance_list_addresses_cancel , Ptr <struct GNUNET_ATS_AddressListHandle> -> IO ()
#ccall GNUNET_ATS_performance_done , Ptr <struct GNUNET_ATS_PerformanceHandle> -> IO ()
#callback GNUNET_ATS_ReservationCallback , Ptr () -> Ptr <struct GNUNET_PeerIdentity> -> CInt -> <struct GNUNET_TIME_Relative> -> IO ()
{- struct GNUNET_ATS_ReservationContext; -}
#opaque_t struct GNUNET_ATS_ReservationContext
#ccall GNUNET_ATS_reserve_bandwidth , Ptr <struct GNUNET_ATS_PerformanceHandle> -> Ptr <struct GNUNET_PeerIdentity> -> CInt -> <GNUNET_ATS_ReservationCallback> -> Ptr () -> IO (Ptr <struct GNUNET_ATS_ReservationContext>)
#ccall GNUNET_ATS_reserve_bandwidth_cancel , Ptr <struct GNUNET_ATS_ReservationContext> -> IO ()
{- enum GNUNET_ATS_PreferenceKind {
    GNUNET_ATS_PREFERENCE_BANDWIDTH = 0,
    GNUNET_ATS_PREFERENCE_LATENCY = 1,
    GNUNET_ATS_PREFERENCE_END = 2
}; -}
#integral_t enum GNUNET_ATS_PreferenceKind
#num GNUNET_ATS_PREFERENCE_BANDWIDTH
#num GNUNET_ATS_PREFERENCE_LATENCY
#num GNUNET_ATS_PREFERENCE_END
#ccall GNUNET_ATS_print_preference_type , <enum GNUNET_ATS_PreferenceKind> -> IO CString
#ccall GNUNET_ATS_performance_change_preference , Ptr <struct GNUNET_ATS_PerformanceHandle> -> Ptr <struct GNUNET_PeerIdentity> -> IO ()
#ccall GNUNET_ATS_performance_give_feedback , Ptr <struct GNUNET_ATS_PerformanceHandle> -> Ptr <struct GNUNET_PeerIdentity> -> <struct GNUNET_TIME_Relative> -> IO ()
