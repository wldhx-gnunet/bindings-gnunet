{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_cadet_service.h"
module Bindings.GNUnet.GnunetCadetService where
import Foreign.Ptr
#strict_import

import Bindings.GNUnet.GnunetCryptoTypes
import Bindings.GNUnet.GnunetConfigurationLib
import Bindings.GNUnet.GnunetMqLib
{- struct GNUNET_CADET_Handle; -}
#opaque_t struct GNUNET_CADET_Handle
{- struct GNUNET_CADET_Channel; -}
#opaque_t struct GNUNET_CADET_Channel
{- struct GNUNET_CADET_Port; -}
#opaque_t struct GNUNET_CADET_Port
{- struct GNUNET_CADET_ClientChannelNumber {
    uint32_t channel_of_client __attribute__((packed));
}; -}
#starttype struct GNUNET_CADET_ClientChannelNumber
#field channel_of_client , CUInt
#stoptype
{- struct GNUNET_CADET_ConnectionTunnelIdentifier {
    struct GNUNET_ShortHashCode connection_of_tunnel;
}; -}
#starttype struct GNUNET_CADET_ConnectionTunnelIdentifier
#field connection_of_tunnel , <struct GNUNET_ShortHashCode>
#stoptype
{- struct GNUNET_CADET_ChannelTunnelNumber {
    uint32_t cn __attribute__((packed));
}; -}
#starttype struct GNUNET_CADET_ChannelTunnelNumber
#field cn , CUInt
#stoptype
{- enum GNUNET_CADET_ChannelOption {
    GNUNET_CADET_OPTION_DEFAULT = 0x0,
    GNUNET_CADET_OPTION_NOBUFFER = 0x1,
    GNUNET_CADET_OPTION_RELIABLE = 0x2,
    GNUNET_CADET_OPTION_OUT_OF_ORDER = 0x4,
    GNUNET_CADET_OPTION_PEER = 0x8,
    GNUNET_CADET_OPTION_CCN = 0x16
}; -}
#integral_t enum GNUNET_CADET_ChannelOption
#num GNUNET_CADET_OPTION_DEFAULT
#num GNUNET_CADET_OPTION_NOBUFFER
#num GNUNET_CADET_OPTION_RELIABLE
#num GNUNET_CADET_OPTION_OUT_OF_ORDER
#num GNUNET_CADET_OPTION_PEER
#num GNUNET_CADET_OPTION_CCN
#callback GNUNET_CADET_ConnectEventHandler , Ptr () -> Ptr <struct GNUNET_CADET_Channel> -> Ptr <struct GNUNET_PeerIdentity> -> IO (Ptr ())
#callback GNUNET_CADET_DisconnectEventHandler , Ptr () -> Ptr <struct GNUNET_CADET_Channel> -> IO ()
#callback GNUNET_CADET_WindowSizeEventHandler , Ptr () -> Ptr <struct GNUNET_CADET_Channel> -> CInt -> IO ()
#ccall GNUNET_CADET_connect , Ptr <struct GNUNET_CONFIGURATION_Handle> -> IO (Ptr <struct GNUNET_CADET_Handle>)
#ccall GNUNET_CADET_disconnect , Ptr <struct GNUNET_CADET_Handle> -> IO ()
#ccall GNUNET_CADET_open_port , Ptr <struct GNUNET_CADET_Handle> -> Ptr <struct GNUNET_HashCode> -> <GNUNET_CADET_ConnectEventHandler> -> Ptr () -> <GNUNET_CADET_WindowSizeEventHandler> -> <GNUNET_CADET_DisconnectEventHandler> -> Ptr <struct GNUNET_MQ_MessageHandler> -> IO (Ptr <struct GNUNET_CADET_Port>)
#ccall GNUNET_CADET_close_port , Ptr <struct GNUNET_CADET_Port> -> IO ()
#ccall GNUNET_CADET_channel_create , Ptr <struct GNUNET_CADET_Handle> -> Ptr () -> Ptr <struct GNUNET_PeerIdentity> -> Ptr <struct GNUNET_HashCode> -> <enum GNUNET_CADET_ChannelOption> -> <GNUNET_CADET_WindowSizeEventHandler> -> <GNUNET_CADET_DisconnectEventHandler> -> Ptr <struct GNUNET_MQ_MessageHandler> -> IO (Ptr <struct GNUNET_CADET_Channel>)
#ccall GNUNET_CADET_channel_destroy , Ptr <struct GNUNET_CADET_Channel> -> IO ()
#ccall GNUNET_CADET_get_mq , Ptr <struct GNUNET_CADET_Channel> -> IO (Ptr <struct GNUNET_MQ_Handle>)
#ccall GNUNET_CADET_receive_done , Ptr <struct GNUNET_CADET_Channel> -> IO ()
#ccall GC_u2h , CUInt -> IO (Ptr <struct GNUNET_HashCode>)
{- union GNUNET_CADET_ChannelInfo {
    int yes_no;
    const struct GNUNET_PeerIdentity peer;
    const struct GNUNET_CADET_ClientChannelNumber ccn;
}; -}
#starttype union GNUNET_CADET_ChannelInfo
#field yes_no , CInt
#field peer , <struct GNUNET_PeerIdentity>
#field ccn , <struct GNUNET_CADET_ClientChannelNumber>
#stoptype
#ccall GNUNET_CADET_channel_get_info , Ptr <struct GNUNET_CADET_Channel> -> <enum GNUNET_CADET_ChannelOption> -> IO (Ptr <union GNUNET_CADET_ChannelInfo>)
{- struct GNUNET_CADET_ChannelInternals {
    struct GNUNET_PeerIdentity root; struct GNUNET_PeerIdentity dest;
}; -}
#starttype struct GNUNET_CADET_ChannelInternals
#field root , <struct GNUNET_PeerIdentity>
#field dest , <struct GNUNET_PeerIdentity>
#stoptype
#callback GNUNET_CADET_ChannelCB , Ptr () -> Ptr <struct GNUNET_CADET_ChannelInternals> -> IO ()
{- struct GNUNET_CADET_ChannelMonitor; -}
#opaque_t struct GNUNET_CADET_ChannelMonitor
#ccall GNUNET_CADET_get_channel , Ptr <struct GNUNET_CONFIGURATION_Handle> -> Ptr <struct GNUNET_PeerIdentity> -> <GNUNET_CADET_ChannelCB> -> Ptr () -> IO (Ptr <struct GNUNET_CADET_ChannelMonitor>)
#ccall GNUNET_CADET_get_channel_cancel , Ptr <struct GNUNET_CADET_ChannelMonitor> -> IO (Ptr ())
{- struct GNUNET_CADET_PeerListEntry {
    struct GNUNET_PeerIdentity peer;
    int have_tunnel;
    unsigned int n_paths;
    unsigned int best_path_length;
}; -}
#starttype struct GNUNET_CADET_PeerListEntry
#field peer , <struct GNUNET_PeerIdentity>
#field have_tunnel , CInt
#field n_paths , CUInt
#field best_path_length , CUInt
#stoptype
#callback GNUNET_CADET_PeersCB , Ptr () -> Ptr <struct GNUNET_CADET_PeerListEntry> -> IO ()
{- struct GNUNET_CADET_PeersLister; -}
#opaque_t struct GNUNET_CADET_PeersLister
#ccall GNUNET_CADET_list_peers , Ptr <struct GNUNET_CONFIGURATION_Handle> -> <GNUNET_CADET_PeersCB> -> Ptr () -> IO (Ptr <struct GNUNET_CADET_PeersLister>)
#ccall GNUNET_CADET_list_peers_cancel , Ptr <struct GNUNET_CADET_PeersLister> -> IO (Ptr ())
{- struct GNUNET_CADET_PeerPathDetail {
    struct GNUNET_PeerIdentity peer;
    unsigned int target_offset;
    unsigned int path_length;
    const struct GNUNET_PeerIdentity * path;
}; -}
#starttype struct GNUNET_CADET_PeerPathDetail
#field peer , <struct GNUNET_PeerIdentity>
#field target_offset , CUInt
#field path_length , CUInt
#field path , Ptr <struct GNUNET_PeerIdentity>
#stoptype
#callback GNUNET_CADET_PathCB , Ptr () -> Ptr <struct GNUNET_CADET_PeerPathDetail> -> IO ()
{- struct GNUNET_CADET_GetPath; -}
#opaque_t struct GNUNET_CADET_GetPath
#ccall GNUNET_CADET_get_path , Ptr <struct GNUNET_CONFIGURATION_Handle> -> Ptr <struct GNUNET_PeerIdentity> -> <GNUNET_CADET_PathCB> -> Ptr () -> IO (Ptr <struct GNUNET_CADET_GetPath>)
#ccall GNUNET_CADET_get_path_cancel , Ptr <struct GNUNET_CADET_GetPath> -> IO (Ptr ())
{- struct GNUNET_CADET_TunnelDetails {
    struct GNUNET_PeerIdentity peer;
    uint32_t channels;
    uint32_t connections;
    uint16_t estate;
    uint16_t cstate;
}; -}
#starttype struct GNUNET_CADET_TunnelDetails
#field peer , <struct GNUNET_PeerIdentity>
#field channels , CUInt
#field connections , CUInt
#field estate , CUShort
#field cstate , CUShort
#stoptype
#callback GNUNET_CADET_TunnelsCB , Ptr () -> Ptr <struct GNUNET_CADET_TunnelDetails> -> IO ()
{- struct GNUNET_CADET_ListTunnels; -}
#opaque_t struct GNUNET_CADET_ListTunnels
#ccall GNUNET_CADET_list_tunnels , Ptr <struct GNUNET_CONFIGURATION_Handle> -> <GNUNET_CADET_TunnelsCB> -> Ptr () -> IO (Ptr <struct GNUNET_CADET_ListTunnels>)
#ccall GNUNET_CADET_list_tunnels_cancel , Ptr <struct GNUNET_CADET_ListTunnels> -> IO (Ptr ())
