{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_mq_lib.h"
module Bindings.GNUnet.GnunetMqLib where
import Foreign.Ptr
#strict_import

import Bindings.GNUnet.GnunetCommon
#ccall GNUNET_MQ_extract_nested_mh_ , Ptr <struct GNUNET_MessageHeader> -> CUShort -> IO (Ptr <struct GNUNET_MessageHeader>)
{- struct GNUNET_MQ_Envelope; -}
#opaque_t struct GNUNET_MQ_Envelope
#ccall GNUNET_MQ_env_get_msg , Ptr <struct GNUNET_MQ_Envelope> -> IO (Ptr <struct GNUNET_MessageHeader>)
#ccall GNUNET_MQ_env_next , Ptr <struct GNUNET_MQ_Envelope> -> IO (Ptr <struct GNUNET_MQ_Envelope>)
#ccall GNUNET_MQ_msg_nested_mh_ , Ptr (Ptr <struct GNUNET_MessageHeader>) -> CUShort -> CUShort -> Ptr <struct GNUNET_MessageHeader> -> IO (Ptr <struct GNUNET_MQ_Envelope>)
{- struct GNUNET_MQ_Handle; -}
#opaque_t struct GNUNET_MQ_Handle
{- enum GNUNET_MQ_Error {
    GNUNET_MQ_ERROR_READ = 1,
    GNUNET_MQ_ERROR_WRITE = 2,
    GNUNET_MQ_ERROR_TIMEOUT = 4,
    GNUNET_MQ_ERROR_MALFORMED = 8,
    GNUNET_MQ_ERROR_NO_MATCH = 16
}; -}
#integral_t enum GNUNET_MQ_Error
#num GNUNET_MQ_ERROR_READ
#num GNUNET_MQ_ERROR_WRITE
#num GNUNET_MQ_ERROR_TIMEOUT
#num GNUNET_MQ_ERROR_MALFORMED
#num GNUNET_MQ_ERROR_NO_MATCH
#callback GNUNET_MQ_MessageCallback , Ptr () -> Ptr <struct GNUNET_MessageHeader> -> IO ()
#callback GNUNET_MQ_MessageValidationCallback , Ptr () -> Ptr <struct GNUNET_MessageHeader> -> IO CInt
#callback GNUNET_MQ_SendImpl , Ptr <struct GNUNET_MQ_Handle> -> Ptr <struct GNUNET_MessageHeader> -> Ptr () -> IO ()
#callback GNUNET_MQ_DestroyImpl , Ptr <struct GNUNET_MQ_Handle> -> Ptr () -> IO ()
#callback GNUNET_MQ_CancelImpl , Ptr <struct GNUNET_MQ_Handle> -> Ptr () -> IO ()
#callback GNUNET_MQ_ErrorHandler , Ptr () -> <enum GNUNET_MQ_Error> -> IO ()
#ccall GNUNET_MQ_dll_insert_tail , Ptr (Ptr <struct GNUNET_MQ_Envelope>) -> Ptr (Ptr <struct GNUNET_MQ_Envelope>) -> Ptr <struct GNUNET_MQ_Envelope> -> IO ()
#ccall GNUNET_MQ_dll_remove , Ptr (Ptr <struct GNUNET_MQ_Envelope>) -> Ptr (Ptr <struct GNUNET_MQ_Envelope>) -> Ptr <struct GNUNET_MQ_Envelope> -> IO ()
#ccall GNUNET_MQ_copy_handlers , Ptr <struct GNUNET_MQ_MessageHandler> -> IO (Ptr <struct GNUNET_MQ_MessageHandler>)
#ccall GNUNET_MQ_copy_handlers2 , Ptr <struct GNUNET_MQ_MessageHandler> -> <GNUNET_MQ_MessageCallback> -> Ptr () -> IO (Ptr <struct GNUNET_MQ_MessageHandler>)
#ccall GNUNET_MQ_count_handlers , Ptr <struct GNUNET_MQ_MessageHandler> -> IO CUInt
{- struct GNUNET_MQ_MessageHandler {
    GNUNET_MQ_MessageValidationCallback mv;
    GNUNET_MQ_MessageCallback cb;
    void * cls;
    uint16_t type;
    uint16_t expected_size;
}; -}
#starttype struct GNUNET_MQ_MessageHandler
#field mv , <GNUNET_MQ_MessageValidationCallback>
#field cb , <GNUNET_MQ_MessageCallback>
#field cls , Ptr ()
#field type , CUShort
#field expected_size , CUShort
#stoptype
#ccall GNUNET_MQ_msg_ , Ptr (Ptr <struct GNUNET_MessageHeader>) -> CUShort -> CUShort -> IO (Ptr <struct GNUNET_MQ_Envelope>)
#ccall GNUNET_MQ_msg_copy , Ptr <struct GNUNET_MessageHeader> -> IO (Ptr <struct GNUNET_MQ_Envelope>)
#ccall GNUNET_MQ_discard , Ptr <struct GNUNET_MQ_Envelope> -> IO ()
#ccall GNUNET_MQ_get_current_envelope , Ptr <struct GNUNET_MQ_Handle> -> IO (Ptr <struct GNUNET_MQ_Envelope>)
#ccall GNUNET_MQ_env_copy , Ptr <struct GNUNET_MQ_Envelope> -> IO (Ptr <struct GNUNET_MQ_Envelope>)
#ccall GNUNET_MQ_get_last_envelope , Ptr <struct GNUNET_MQ_Handle> -> IO (Ptr <struct GNUNET_MQ_Envelope>)
#ccall GNUNET_MQ_env_set_options , Ptr <struct GNUNET_MQ_Envelope> -> CULong -> Ptr () -> IO ()
#ccall GNUNET_MQ_env_get_options , Ptr <struct GNUNET_MQ_Envelope> -> Ptr CULong -> IO (Ptr ())
#ccall GNUNET_MQ_unsent_head , Ptr <struct GNUNET_MQ_Handle> -> IO (Ptr <struct GNUNET_MQ_Envelope>)
#ccall GNUNET_MQ_set_options , Ptr <struct GNUNET_MQ_Handle> -> CULong -> Ptr () -> IO ()
#ccall GNUNET_MQ_get_length , Ptr <struct GNUNET_MQ_Handle> -> IO CUInt
#ccall GNUNET_MQ_send , Ptr <struct GNUNET_MQ_Handle> -> Ptr <struct GNUNET_MQ_Envelope> -> IO ()
#ccall GNUNET_MQ_send_copy , Ptr <struct GNUNET_MQ_Handle> -> Ptr <struct GNUNET_MQ_Envelope> -> IO ()
#ccall GNUNET_MQ_send_cancel , Ptr <struct GNUNET_MQ_Envelope> -> IO ()
#ccall GNUNET_MQ_assoc_add , Ptr <struct GNUNET_MQ_Handle> -> Ptr () -> IO CUInt
#ccall GNUNET_MQ_assoc_get , Ptr <struct GNUNET_MQ_Handle> -> CUInt -> IO (Ptr ())
#ccall GNUNET_MQ_assoc_remove , Ptr <struct GNUNET_MQ_Handle> -> CUInt -> IO (Ptr ())
#ccall GNUNET_MQ_queue_for_callbacks , <GNUNET_MQ_SendImpl> -> <GNUNET_MQ_DestroyImpl> -> <GNUNET_MQ_CancelImpl> -> Ptr () -> Ptr <struct GNUNET_MQ_MessageHandler> -> <GNUNET_MQ_ErrorHandler> -> Ptr () -> IO (Ptr <struct GNUNET_MQ_Handle>)
#ccall GNUNET_MQ_set_handlers_closure , Ptr <struct GNUNET_MQ_Handle> -> Ptr () -> IO ()
#ccall GNUNET_MQ_destroy , Ptr <struct GNUNET_MQ_Handle> -> IO ()
{- struct GNUNET_MQ_DestroyNotificationHandle; -}
#opaque_t struct GNUNET_MQ_DestroyNotificationHandle
#ccall GNUNET_MQ_destroy_notify_cancel , Ptr <struct GNUNET_MQ_DestroyNotificationHandle> -> IO ()
#ccall GNUNET_MQ_inject_message , Ptr <struct GNUNET_MQ_Handle> -> Ptr <struct GNUNET_MessageHeader> -> IO ()
#ccall GNUNET_MQ_inject_error , Ptr <struct GNUNET_MQ_Handle> -> <enum GNUNET_MQ_Error> -> IO ()
#ccall GNUNET_MQ_impl_send_continue , Ptr <struct GNUNET_MQ_Handle> -> IO ()
#ccall GNUNET_MQ_impl_send_in_flight , Ptr <struct GNUNET_MQ_Handle> -> IO ()
#ccall GNUNET_MQ_impl_state , Ptr <struct GNUNET_MQ_Handle> -> IO (Ptr ())
#ccall GNUNET_MQ_impl_current , Ptr <struct GNUNET_MQ_Handle> -> IO (Ptr <struct GNUNET_MessageHeader>)
