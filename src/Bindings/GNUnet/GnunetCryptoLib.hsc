{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_crypto_lib.h"
module Bindings.GNUnet.GnunetCryptoLib where
import Foreign.Ptr
#strict_import

import Bindings.GNUnet.GnunetCryptoTypes
import Bindings.GNUnet.GnunetCommon
import Bindings.GNUnet.GnunetConfigurationLib (C'GNUNET_CONFIGURATION_Handle)
#ccall GNUNET_CRYPTO_seed_weak_random , CInt -> IO ()
#ccall GNUNET_CRYPTO_crc8_n , Ptr () -> CSize -> IO CUChar
#ccall GNUNET_CRYPTO_crc16_step , CUInt -> Ptr () -> CSize -> IO CUInt
#ccall GNUNET_CRYPTO_crc16_finish , CUInt -> IO CUShort
#ccall GNUNET_CRYPTO_crc16_n , Ptr () -> CSize -> IO CUShort
#ccall GNUNET_CRYPTO_crc32_n , Ptr () -> CSize -> IO CInt
#ccall GNUNET_CRYPTO_random_block , <enum GNUNET_CRYPTO_Quality> -> Ptr () -> CSize -> IO ()
#ccall GNUNET_CRYPTO_random_u32 , <enum GNUNET_CRYPTO_Quality> -> CUInt -> IO CUInt
#ccall GNUNET_CRYPTO_random_u64 , <enum GNUNET_CRYPTO_Quality> -> CULong -> IO CULong
#ccall GNUNET_CRYPTO_random_permute , <enum GNUNET_CRYPTO_Quality> -> CUInt -> IO (Ptr CUInt)
#ccall GNUNET_CRYPTO_symmetric_create_session_key , Ptr <struct GNUNET_CRYPTO_SymmetricSessionKey> -> IO ()
#ccall GNUNET_CRYPTO_symmetric_encrypt , Ptr () -> CSize -> Ptr <struct GNUNET_CRYPTO_SymmetricSessionKey> -> Ptr <struct GNUNET_CRYPTO_SymmetricInitializationVector> -> Ptr () -> IO CLong
#ccall GNUNET_CRYPTO_symmetric_decrypt , Ptr () -> CSize -> Ptr <struct GNUNET_CRYPTO_SymmetricSessionKey> -> Ptr <struct GNUNET_CRYPTO_SymmetricInitializationVector> -> Ptr () -> IO CLong
#ccall GNUNET_CRYPTO_symmetric_derive_iv , Ptr <struct GNUNET_CRYPTO_SymmetricInitializationVector> -> Ptr <struct GNUNET_CRYPTO_SymmetricSessionKey> -> Ptr () -> CSize -> IO ()
#ccall GNUNET_CRYPTO_hash_to_enc , Ptr <struct GNUNET_HashCode> -> Ptr <struct GNUNET_CRYPTO_HashAsciiEncoded> -> IO ()
#ccall GNUNET_CRYPTO_hash_from_string2 , CString -> CSize -> Ptr <struct GNUNET_HashCode> -> IO CInt
#ccall GNUNET_CRYPTO_hash_distance_u32 , Ptr <struct GNUNET_HashCode> -> Ptr <struct GNUNET_HashCode> -> IO CUInt
#ccall GNUNET_CRYPTO_hash , Ptr () -> CSize -> Ptr <struct GNUNET_HashCode> -> IO ()
{- struct GNUNET_HashContext; -}
#opaque_t struct GNUNET_HashContext
#ccall GNUNET_CRYPTO_hash_context_start , IO (Ptr <struct GNUNET_HashContext>)
#ccall GNUNET_CRYPTO_hash_context_read , Ptr <struct GNUNET_HashContext> -> Ptr () -> CSize -> IO ()
#ccall GNUNET_CRYPTO_hash_context_finish , Ptr <struct GNUNET_HashContext> -> Ptr <struct GNUNET_HashCode> -> IO ()
#ccall GNUNET_CRYPTO_hash_context_abort , Ptr <struct GNUNET_HashContext> -> IO ()
#ccall GNUNET_CRYPTO_hmac_raw , Ptr () -> CSize -> Ptr () -> CSize -> Ptr <struct GNUNET_HashCode> -> IO ()
#ccall GNUNET_CRYPTO_hmac , Ptr <struct GNUNET_CRYPTO_AuthKey> -> Ptr () -> CSize -> Ptr <struct GNUNET_HashCode> -> IO ()
#callback GNUNET_CRYPTO_HashCompletedCallback , Ptr () -> Ptr <struct GNUNET_HashCode> -> IO ()
{- struct GNUNET_CRYPTO_FileHashContext; -}
#opaque_t struct GNUNET_CRYPTO_FileHashContext
#ccall GNUNET_CRYPTO_hash_file , <enum GNUNET_SCHEDULER_Priority> -> CString -> CSize -> <GNUNET_CRYPTO_HashCompletedCallback> -> Ptr () -> IO (Ptr <struct GNUNET_CRYPTO_FileHashContext>)
#ccall GNUNET_CRYPTO_hash_file_cancel , Ptr <struct GNUNET_CRYPTO_FileHashContext> -> IO ()
#ccall GNUNET_CRYPTO_hash_create_random , <enum GNUNET_CRYPTO_Quality> -> Ptr <struct GNUNET_HashCode> -> IO ()
#ccall GNUNET_CRYPTO_hash_difference , Ptr <struct GNUNET_HashCode> -> Ptr <struct GNUNET_HashCode> -> Ptr <struct GNUNET_HashCode> -> IO ()
#ccall GNUNET_CRYPTO_hash_sum , Ptr <struct GNUNET_HashCode> -> Ptr <struct GNUNET_HashCode> -> Ptr <struct GNUNET_HashCode> -> IO ()
#ccall GNUNET_CRYPTO_hash_xor , Ptr <struct GNUNET_HashCode> -> Ptr <struct GNUNET_HashCode> -> Ptr <struct GNUNET_HashCode> -> IO ()
#ccall GNUNET_CRYPTO_hash_to_aes_key , Ptr <struct GNUNET_HashCode> -> Ptr <struct GNUNET_CRYPTO_SymmetricSessionKey> -> Ptr <struct GNUNET_CRYPTO_SymmetricInitializationVector> -> IO ()
#ccall GNUNET_CRYPTO_hash_get_bit , Ptr <struct GNUNET_HashCode> -> CUInt -> IO CInt
#ccall GNUNET_CRYPTO_hash_matching_bits , Ptr <struct GNUNET_HashCode> -> Ptr <struct GNUNET_HashCode> -> IO CUInt
#ccall GNUNET_CRYPTO_hash_cmp , Ptr <struct GNUNET_HashCode> -> Ptr <struct GNUNET_HashCode> -> IO CInt
#ccall GNUNET_CRYPTO_hash_xorcmp , Ptr <struct GNUNET_HashCode> -> Ptr <struct GNUNET_HashCode> -> Ptr <struct GNUNET_HashCode> -> IO CInt
#ccall GNUNET_CRYPTO_hmac_derive_key , Ptr <struct GNUNET_CRYPTO_AuthKey> -> Ptr <struct GNUNET_CRYPTO_SymmetricSessionKey> -> Ptr () -> CSize -> IO ()
#ccall GNUNET_CRYPTO_hkdf , Ptr () -> CSize -> CInt -> CInt -> Ptr () -> CSize -> Ptr () -> CSize -> IO CInt
#ccall GNUNET_CRYPTO_kdf , Ptr () -> CSize -> Ptr () -> CSize -> Ptr () -> CSize -> IO CInt
#ccall GNUNET_CRYPTO_ecdsa_key_get_public , Ptr <struct GNUNET_CRYPTO_EcdsaPrivateKey> -> Ptr <struct GNUNET_CRYPTO_EcdsaPublicKey> -> IO ()
#ccall GNUNET_CRYPTO_eddsa_key_get_public , Ptr <struct GNUNET_CRYPTO_EddsaPrivateKey> -> Ptr <struct GNUNET_CRYPTO_EddsaPublicKey> -> IO ()
#ccall GNUNET_CRYPTO_ecdhe_key_get_public , Ptr <struct GNUNET_CRYPTO_EcdhePrivateKey> -> Ptr <struct GNUNET_CRYPTO_EcdhePublicKey> -> IO ()
#ccall GNUNET_CRYPTO_ecdsa_public_key_to_string , Ptr <struct GNUNET_CRYPTO_EcdsaPublicKey> -> IO CString
#ccall GNUNET_CRYPTO_eddsa_private_key_to_string , Ptr <struct GNUNET_CRYPTO_EddsaPrivateKey> -> IO CString
#ccall GNUNET_CRYPTO_eddsa_public_key_to_string , Ptr <struct GNUNET_CRYPTO_EddsaPublicKey> -> IO CString
#ccall GNUNET_CRYPTO_ecdsa_public_key_from_string , CString -> CSize -> Ptr <struct GNUNET_CRYPTO_EcdsaPublicKey> -> IO CInt
#ccall GNUNET_CRYPTO_eddsa_private_key_from_string , CString -> CSize -> Ptr <struct GNUNET_CRYPTO_EddsaPrivateKey> -> IO CInt
#ccall GNUNET_CRYPTO_eddsa_public_key_from_string , CString -> CSize -> Ptr <struct GNUNET_CRYPTO_EddsaPublicKey> -> IO CInt
#ccall GNUNET_CRYPTO_ecdsa_key_create_from_file , CString -> IO (Ptr <struct GNUNET_CRYPTO_EcdsaPrivateKey>)
#ccall GNUNET_CRYPTO_eddsa_key_create_from_file , CString -> IO (Ptr <struct GNUNET_CRYPTO_EddsaPrivateKey>)
#ccall GNUNET_CRYPTO_eddsa_key_create_from_configuration , Ptr <struct GNUNET_CONFIGURATION_Handle> -> IO (Ptr <struct GNUNET_CRYPTO_EddsaPrivateKey>)
#ccall GNUNET_CRYPTO_ecdsa_key_create , IO (Ptr <struct GNUNET_CRYPTO_EcdsaPrivateKey>)
#ccall GNUNET_CRYPTO_eddsa_key_create , IO (Ptr <struct GNUNET_CRYPTO_EddsaPrivateKey>)
#ccall GNUNET_CRYPTO_ecdhe_key_create2 , Ptr <struct GNUNET_CRYPTO_EcdhePrivateKey> -> IO CInt
#ccall GNUNET_CRYPTO_ecdhe_key_create , IO (Ptr <struct GNUNET_CRYPTO_EcdhePrivateKey>)
#ccall GNUNET_CRYPTO_eddsa_key_clear , Ptr <struct GNUNET_CRYPTO_EddsaPrivateKey> -> IO ()
#ccall GNUNET_CRYPTO_ecdsa_key_clear , Ptr <struct GNUNET_CRYPTO_EcdsaPrivateKey> -> IO ()
#ccall GNUNET_CRYPTO_ecdhe_key_clear , Ptr <struct GNUNET_CRYPTO_EcdhePrivateKey> -> IO ()
#ccall GNUNET_CRYPTO_ecdsa_key_get_anonymous , IO (Ptr <struct GNUNET_CRYPTO_EcdsaPrivateKey>)
#ccall GNUNET_CRYPTO_get_peer_identity , Ptr <struct GNUNET_CONFIGURATION_Handle> -> Ptr <struct GNUNET_PeerIdentity> -> IO CInt
#ccall GNUNET_CRYPTO_cmp_peer_identity , Ptr <struct GNUNET_PeerIdentity> -> Ptr <struct GNUNET_PeerIdentity> -> IO CInt
{- struct GNUNET_CRYPTO_EccDlogContext; -}
#opaque_t struct GNUNET_CRYPTO_EccDlogContext
{- struct GNUNET_CRYPTO_EccPoint {
    unsigned char q_y[256 / 8];
}; -}
#starttype struct GNUNET_CRYPTO_EccPoint
#array_field q_y , CUChar
#stoptype
#ccall GNUNET_CRYPTO_ecc_dlog_prepare , CUInt -> CUInt -> IO (Ptr <struct GNUNET_CRYPTO_EccDlogContext>)
#ccall GNUNET_CRYPTO_ecc_dlog_release , Ptr <struct GNUNET_CRYPTO_EccDlogContext> -> IO ()
#ccall GNUNET_CRYPTO_ecc_ecdh , Ptr <struct GNUNET_CRYPTO_EcdhePrivateKey> -> Ptr <struct GNUNET_CRYPTO_EcdhePublicKey> -> Ptr <struct GNUNET_HashCode> -> IO CInt
#ccall GNUNET_CRYPTO_eddsa_ecdh , Ptr <struct GNUNET_CRYPTO_EddsaPrivateKey> -> Ptr <struct GNUNET_CRYPTO_EcdhePublicKey> -> Ptr <struct GNUNET_HashCode> -> IO CInt
#ccall GNUNET_CRYPTO_ecdsa_ecdh , Ptr <struct GNUNET_CRYPTO_EcdsaPrivateKey> -> Ptr <struct GNUNET_CRYPTO_EcdhePublicKey> -> Ptr <struct GNUNET_HashCode> -> IO CInt
#ccall GNUNET_CRYPTO_ecdh_eddsa , Ptr <struct GNUNET_CRYPTO_EcdhePrivateKey> -> Ptr <struct GNUNET_CRYPTO_EddsaPublicKey> -> Ptr <struct GNUNET_HashCode> -> IO CInt
#ccall GNUNET_CRYPTO_ecdh_ecdsa , Ptr <struct GNUNET_CRYPTO_EcdhePrivateKey> -> Ptr <struct GNUNET_CRYPTO_EcdsaPublicKey> -> Ptr <struct GNUNET_HashCode> -> IO CInt
#ccall GNUNET_CRYPTO_eddsa_sign , Ptr <struct GNUNET_CRYPTO_EddsaPrivateKey> -> Ptr <struct GNUNET_CRYPTO_EccSignaturePurpose> -> Ptr <struct GNUNET_CRYPTO_EddsaSignature> -> IO CInt
#ccall GNUNET_CRYPTO_ecdsa_sign , Ptr <struct GNUNET_CRYPTO_EcdsaPrivateKey> -> Ptr <struct GNUNET_CRYPTO_EccSignaturePurpose> -> Ptr <struct GNUNET_CRYPTO_EcdsaSignature> -> IO CInt
#ccall GNUNET_CRYPTO_eddsa_verify , CUInt -> Ptr <struct GNUNET_CRYPTO_EccSignaturePurpose> -> Ptr <struct GNUNET_CRYPTO_EddsaSignature> -> Ptr <struct GNUNET_CRYPTO_EddsaPublicKey> -> IO CInt
#ccall GNUNET_CRYPTO_ecdsa_verify , CUInt -> Ptr <struct GNUNET_CRYPTO_EccSignaturePurpose> -> Ptr <struct GNUNET_CRYPTO_EcdsaSignature> -> Ptr <struct GNUNET_CRYPTO_EcdsaPublicKey> -> IO CInt
#ccall GNUNET_CRYPTO_ecdsa_private_key_derive , Ptr <struct GNUNET_CRYPTO_EcdsaPrivateKey> -> CString -> CString -> IO (Ptr <struct GNUNET_CRYPTO_EcdsaPrivateKey>)
#ccall GNUNET_CRYPTO_ecdsa_public_key_derive , Ptr <struct GNUNET_CRYPTO_EcdsaPublicKey> -> CString -> CString -> Ptr <struct GNUNET_CRYPTO_EcdsaPublicKey> -> IO ()
#ccall GNUNET_CRYPTO_paillier_create , Ptr <struct GNUNET_CRYPTO_PaillierPublicKey> -> Ptr <struct GNUNET_CRYPTO_PaillierPrivateKey> -> IO ()
#ccall GNUNET_CRYPTO_paillier_hom_add , Ptr <struct GNUNET_CRYPTO_PaillierPublicKey> -> Ptr <struct GNUNET_CRYPTO_PaillierCiphertext> -> Ptr <struct GNUNET_CRYPTO_PaillierCiphertext> -> Ptr <struct GNUNET_CRYPTO_PaillierCiphertext> -> IO CInt
#ccall GNUNET_CRYPTO_paillier_hom_get_remaining , Ptr <struct GNUNET_CRYPTO_PaillierCiphertext> -> IO CInt
{- struct GNUNET_CRYPTO_RsaPrivateKey; -}
#opaque_t struct GNUNET_CRYPTO_RsaPrivateKey
{- struct GNUNET_CRYPTO_RsaPublicKey; -}
#opaque_t struct GNUNET_CRYPTO_RsaPublicKey
{- struct GNUNET_CRYPTO_RsaBlindingKeySecret {
    uint32_t pre_secret[8] __attribute__((packed));
}; -}
#starttype struct GNUNET_CRYPTO_RsaBlindingKeySecret
#array_field pre_secret , CUInt
#stoptype
{- struct GNUNET_CRYPTO_RsaSignature; -}
#opaque_t struct GNUNET_CRYPTO_RsaSignature
#ccall GNUNET_CRYPTO_rsa_private_key_create , CUInt -> IO (Ptr <struct GNUNET_CRYPTO_RsaPrivateKey>)
#ccall GNUNET_CRYPTO_rsa_private_key_free , Ptr <struct GNUNET_CRYPTO_RsaPrivateKey> -> IO ()
#ccall GNUNET_CRYPTO_rsa_private_key_encode , Ptr <struct GNUNET_CRYPTO_RsaPrivateKey> -> Ptr CString -> IO CSize
#ccall GNUNET_CRYPTO_rsa_private_key_decode , CString -> CSize -> IO (Ptr <struct GNUNET_CRYPTO_RsaPrivateKey>)
#ccall GNUNET_CRYPTO_rsa_private_key_dup , Ptr <struct GNUNET_CRYPTO_RsaPrivateKey> -> IO (Ptr <struct GNUNET_CRYPTO_RsaPrivateKey>)
#ccall GNUNET_CRYPTO_rsa_private_key_get_public , Ptr <struct GNUNET_CRYPTO_RsaPrivateKey> -> IO (Ptr <struct GNUNET_CRYPTO_RsaPublicKey>)
#ccall GNUNET_CRYPTO_rsa_public_key_hash , Ptr <struct GNUNET_CRYPTO_RsaPublicKey> -> Ptr <struct GNUNET_HashCode> -> IO ()
#ccall GNUNET_CRYPTO_rsa_public_key_len , Ptr <struct GNUNET_CRYPTO_RsaPublicKey> -> IO CUInt
#ccall GNUNET_CRYPTO_rsa_public_key_free , Ptr <struct GNUNET_CRYPTO_RsaPublicKey> -> IO ()
#ccall GNUNET_CRYPTO_rsa_public_key_encode , Ptr <struct GNUNET_CRYPTO_RsaPublicKey> -> Ptr CString -> IO CSize
#ccall GNUNET_CRYPTO_rsa_public_key_decode , CString -> CSize -> IO (Ptr <struct GNUNET_CRYPTO_RsaPublicKey>)
#ccall GNUNET_CRYPTO_rsa_public_key_dup , Ptr <struct GNUNET_CRYPTO_RsaPublicKey> -> IO (Ptr <struct GNUNET_CRYPTO_RsaPublicKey>)
#ccall GNUNET_CRYPTO_rsa_signature_cmp , Ptr <struct GNUNET_CRYPTO_RsaSignature> -> Ptr <struct GNUNET_CRYPTO_RsaSignature> -> IO CInt
#ccall GNUNET_CRYPTO_rsa_private_key_cmp , Ptr <struct GNUNET_CRYPTO_RsaPrivateKey> -> Ptr <struct GNUNET_CRYPTO_RsaPrivateKey> -> IO CInt
#ccall GNUNET_CRYPTO_rsa_public_key_cmp , Ptr <struct GNUNET_CRYPTO_RsaPublicKey> -> Ptr <struct GNUNET_CRYPTO_RsaPublicKey> -> IO CInt
#ccall GNUNET_CRYPTO_rsa_blind , Ptr <struct GNUNET_HashCode> -> Ptr <struct GNUNET_CRYPTO_RsaBlindingKeySecret> -> Ptr <struct GNUNET_CRYPTO_RsaPublicKey> -> Ptr CString -> Ptr CSize -> IO CInt
#ccall GNUNET_CRYPTO_rsa_sign_blinded , Ptr <struct GNUNET_CRYPTO_RsaPrivateKey> -> Ptr () -> CSize -> IO (Ptr <struct GNUNET_CRYPTO_RsaSignature>)
#ccall GNUNET_CRYPTO_rsa_sign_fdh , Ptr <struct GNUNET_CRYPTO_RsaPrivateKey> -> Ptr <struct GNUNET_HashCode> -> IO (Ptr <struct GNUNET_CRYPTO_RsaSignature>)
#ccall GNUNET_CRYPTO_rsa_signature_free , Ptr <struct GNUNET_CRYPTO_RsaSignature> -> IO ()
#ccall GNUNET_CRYPTO_rsa_signature_encode , Ptr <struct GNUNET_CRYPTO_RsaSignature> -> Ptr CString -> IO CSize
#ccall GNUNET_CRYPTO_rsa_signature_decode , CString -> CSize -> IO (Ptr <struct GNUNET_CRYPTO_RsaSignature>)
#ccall GNUNET_CRYPTO_rsa_signature_dup , Ptr <struct GNUNET_CRYPTO_RsaSignature> -> IO (Ptr <struct GNUNET_CRYPTO_RsaSignature>)
#ccall GNUNET_CRYPTO_rsa_unblind , Ptr <struct GNUNET_CRYPTO_RsaSignature> -> Ptr <struct GNUNET_CRYPTO_RsaBlindingKeySecret> -> Ptr <struct GNUNET_CRYPTO_RsaPublicKey> -> IO (Ptr <struct GNUNET_CRYPTO_RsaSignature>)
#ccall GNUNET_CRYPTO_rsa_verify , Ptr <struct GNUNET_HashCode> -> Ptr <struct GNUNET_CRYPTO_RsaSignature> -> Ptr <struct GNUNET_CRYPTO_RsaPublicKey> -> IO CInt
