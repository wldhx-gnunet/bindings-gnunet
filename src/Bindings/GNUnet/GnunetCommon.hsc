{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_common.h"
module Bindings.GNUnet.GnunetCommon where
import Foreign.Ptr
#strict_import

import Bindings.GNUnet.GnunetCryptoTypes

{- struct sockaddr; -}
#opaque_t struct sockaddr

#num_pattern GNUNET_OK
#num_pattern GNUNET_SYSERR
#num_pattern GNUNET_YES
#num_pattern GNUNET_NO
{- struct GNUNET_MessageHeader {
    uint16_t size __attribute__((packed));
    uint16_t type __attribute__((packed));
}; -}
#starttype struct GNUNET_MessageHeader
#field size , CUShort
#field type , CUShort
#stoptype
{- struct GNUNET_OperationResultMessage {
    struct GNUNET_MessageHeader header;
    uint32_t reserved __attribute__((packed));
    uint64_t op_id __attribute__((packed));
    uint64_t result_code __attribute__((packed));
}; -}
#starttype struct GNUNET_OperationResultMessage
#field header , <struct GNUNET_MessageHeader>
#field reserved , CUInt
#field op_id , CULong
#field result_code , CULong
#stoptype
#callback GNUNET_FileNameCallback , Ptr () -> CString -> IO CInt
#callback GNUNET_ContinuationCallback , Ptr () -> IO ()
#callback GNUNET_ResultCallback , Ptr () -> CLong -> Ptr () -> CUShort -> IO ()
{- enum GNUNET_ErrorType {
    GNUNET_ERROR_TYPE_UNSPECIFIED = -1,
    GNUNET_ERROR_TYPE_NONE = 0,
    GNUNET_ERROR_TYPE_ERROR = 1,
    GNUNET_ERROR_TYPE_WARNING = 2,
    GNUNET_ERROR_TYPE_MESSAGE = 4,
    GNUNET_ERROR_TYPE_INFO = 8,
    GNUNET_ERROR_TYPE_DEBUG = 16,
    GNUNET_ERROR_TYPE_INVALID = 32,
    GNUNET_ERROR_TYPE_BULK = 64
}; -}
#integral_t enum GNUNET_ErrorType
#num GNUNET_ERROR_TYPE_UNSPECIFIED
#num GNUNET_ERROR_TYPE_NONE
#num GNUNET_ERROR_TYPE_ERROR
#num GNUNET_ERROR_TYPE_WARNING
#num GNUNET_ERROR_TYPE_MESSAGE
#num GNUNET_ERROR_TYPE_INFO
#num GNUNET_ERROR_TYPE_DEBUG
#num GNUNET_ERROR_TYPE_INVALID
#num GNUNET_ERROR_TYPE_BULK
#callback GNUNET_Logger , Ptr () -> <enum GNUNET_ErrorType> -> CString -> CString -> CString -> IO ()
#ccall GNUNET_get_log_skip , IO CInt
#ccall GNUNET_get_log_call_status , CInt -> CString -> CString -> CString -> CInt -> IO CInt
#ccall GNUNET_log_nocheck , <enum GNUNET_ErrorType> -> CString -> IO ()
#ccall GNUNET_log_from_nocheck , <enum GNUNET_ErrorType> -> CString -> CString -> IO ()
#ccall GNUNET_log_config_missing , <enum GNUNET_ErrorType> -> CString -> CString -> IO ()
#ccall GNUNET_log_config_invalid , <enum GNUNET_ErrorType> -> CString -> CString -> CString -> IO ()
#ccall GNUNET_abort_ , IO ()
#ccall GNUNET_log_skip , CInt -> CInt -> IO ()
#ccall GNUNET_log_setup , CString -> CString -> CString -> IO CInt
#ccall GNUNET_logger_add , <GNUNET_Logger> -> Ptr () -> IO ()
#ccall GNUNET_logger_remove , <GNUNET_Logger> -> Ptr () -> IO ()
#ccall GNUNET_sh2s , Ptr <struct GNUNET_ShortHashCode> -> IO CString
#ccall GNUNET_h2s , Ptr <struct GNUNET_HashCode> -> IO CString
#ccall GNUNET_h2s2 , Ptr <struct GNUNET_HashCode> -> IO CString
#ccall GNUNET_h2s_full , Ptr <struct GNUNET_HashCode> -> IO CString
#ccall GNUNET_p2s , Ptr <struct GNUNET_CRYPTO_EddsaPublicKey> -> IO CString
#ccall GNUNET_p2s2 , Ptr <struct GNUNET_CRYPTO_EddsaPublicKey> -> IO CString
#ccall GNUNET_e2s , Ptr <struct GNUNET_CRYPTO_EcdhePublicKey> -> IO CString
#ccall GNUNET_e2s2 , Ptr <struct GNUNET_CRYPTO_EcdhePublicKey> -> IO CString
#ccall GNUNET_i2s , Ptr <struct GNUNET_PeerIdentity> -> IO CString
#ccall GNUNET_i2s2 , Ptr <struct GNUNET_PeerIdentity> -> IO CString
#ccall GNUNET_i2s_full , Ptr <struct GNUNET_PeerIdentity> -> IO CString
#ccall GNUNET_a2s , Ptr <struct sockaddr> -> CUInt -> IO CString
#ccall GNUNET_error_type_to_string , <enum GNUNET_ErrorType> -> IO CString
-- not really GNUnet-specific
#ccall ntohs, CUShort -> CUShort
#ccall GNUNET_htonll , CULong -> IO CULong
#ccall GNUNET_ntohll , CULong -> IO CULong
#ccall GNUNET_hton_double , CDouble -> IO CDouble
#ccall GNUNET_ntoh_double , CDouble -> IO CDouble
#ccall GNUNET_snprintf , CString -> CSize -> CString -> IO CInt
#ccall GNUNET_asprintf , Ptr CString -> CString -> IO CInt
#ccall GNUNET_xmalloc_ , CSize -> CString -> CInt -> IO (Ptr ())
#ccall GNUNET_xnew_array_2d_ , CSize -> CSize -> CSize -> CString -> CInt -> IO (Ptr (Ptr ()))
#ccall GNUNET_xnew_array_3d_ , CSize -> CSize -> CSize -> CSize -> CString -> CInt -> IO (Ptr (Ptr (Ptr ())))
#ccall GNUNET_xmemdup_ , Ptr () -> CSize -> CString -> CInt -> IO (Ptr ())
#ccall GNUNET_xmalloc_unchecked_ , CSize -> CString -> CInt -> IO (Ptr ())
#ccall GNUNET_xrealloc_ , Ptr () -> CSize -> CString -> CInt -> IO (Ptr ())
#ccall GNUNET_xfree_ , Ptr () -> CString -> CInt -> IO ()
#ccall GNUNET_xstrdup_ , CString -> CString -> CInt -> IO CString
#ccall GNUNET_xstrndup_ , CString -> CSize -> CString -> CInt -> IO CString
#ccall GNUNET_xgrow_ , Ptr (Ptr ()) -> CSize -> Ptr CUInt -> CUInt -> CString -> CInt -> IO ()
#ccall GNUNET_copy_message , Ptr <struct GNUNET_MessageHeader> -> IO (Ptr <struct GNUNET_MessageHeader>)
{- enum GNUNET_SCHEDULER_Priority {
    GNUNET_SCHEDULER_PRIORITY_KEEP = 0,
    GNUNET_SCHEDULER_PRIORITY_IDLE = 1,
    GNUNET_SCHEDULER_PRIORITY_BACKGROUND = 2,
    GNUNET_SCHEDULER_PRIORITY_DEFAULT = 3,
    GNUNET_SCHEDULER_PRIORITY_HIGH = 4,
    GNUNET_SCHEDULER_PRIORITY_UI = 5,
    GNUNET_SCHEDULER_PRIORITY_URGENT = 6,
    GNUNET_SCHEDULER_PRIORITY_SHUTDOWN = 7,
    GNUNET_SCHEDULER_PRIORITY_COUNT = 8
}; -}
#integral_t enum GNUNET_SCHEDULER_Priority
#num GNUNET_SCHEDULER_PRIORITY_KEEP
#num GNUNET_SCHEDULER_PRIORITY_IDLE
#num GNUNET_SCHEDULER_PRIORITY_BACKGROUND
#num GNUNET_SCHEDULER_PRIORITY_DEFAULT
#num GNUNET_SCHEDULER_PRIORITY_HIGH
#num GNUNET_SCHEDULER_PRIORITY_UI
#num GNUNET_SCHEDULER_PRIORITY_URGENT
#num GNUNET_SCHEDULER_PRIORITY_SHUTDOWN
#num GNUNET_SCHEDULER_PRIORITY_COUNT
