{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_network_lib.h"
module Bindings.GNUnet.GnunetNetworkLib where
import Foreign.Ptr
#strict_import

import Bindings.GNUnet.GnunetDiskLib
import Bindings.GNUnet.GnunetTimeLib
import Bindings.GNUnet.Internal.Select
#opaque_t struct sockaddr
#opaque_t struct sockaddr_un

{- struct GNUNET_NETWORK_Handle; -}
#opaque_t struct GNUNET_NETWORK_Handle
{- struct GNUNET_NETWORK_FDSet {
    int nsds; fd_set sds;
}; -}
#starttype struct GNUNET_NETWORK_FDSet
#field nsds , CInt
#field sds , <fd_set>
#stoptype
#ccall GNUNET_NETWORK_test_pf , CInt -> IO CInt
#ccall GNUNET_NETWORK_shorten_unixpath , CString -> IO CString
#ccall GNUNET_NETWORK_unix_precheck , Ptr <struct sockaddr_un> -> IO ()
#ccall GNUNET_NETWORK_socket_accept , Ptr <struct GNUNET_NETWORK_Handle> -> Ptr <struct sockaddr> -> Ptr CUInt -> IO (Ptr <struct GNUNET_NETWORK_Handle>)
#ccall GNUNET_NETWORK_socket_box_native , CInt -> IO (Ptr <struct GNUNET_NETWORK_Handle>)
#ccall GNUNET_NETWORK_socket_set_blocking , Ptr <struct GNUNET_NETWORK_Handle> -> CInt -> IO CInt
#ccall GNUNET_NETWORK_socket_bind , Ptr <struct GNUNET_NETWORK_Handle> -> Ptr <struct sockaddr> -> CUInt -> IO CInt
#ccall GNUNET_NETWORK_socket_close , Ptr <struct GNUNET_NETWORK_Handle> -> IO CInt
#ccall GNUNET_NETWORK_socket_free_memory_only_ , Ptr <struct GNUNET_NETWORK_Handle> -> IO ()
#ccall GNUNET_NETWORK_socket_connect , Ptr <struct GNUNET_NETWORK_Handle> -> Ptr <struct sockaddr> -> CUInt -> IO CInt
#ccall GNUNET_NETWORK_socket_getsockopt , Ptr <struct GNUNET_NETWORK_Handle> -> CInt -> CInt -> Ptr () -> Ptr CUInt -> IO CInt
#ccall GNUNET_NETWORK_socket_listen , Ptr <struct GNUNET_NETWORK_Handle> -> CInt -> IO CInt
#ccall GNUNET_NETWORK_socket_recvfrom_amount , Ptr <struct GNUNET_NETWORK_Handle> -> IO CLong
#ccall GNUNET_NETWORK_socket_recvfrom , Ptr <struct GNUNET_NETWORK_Handle> -> Ptr () -> CSize -> Ptr <struct sockaddr> -> Ptr CUInt -> IO CLong
#ccall GNUNET_NETWORK_socket_recv , Ptr <struct GNUNET_NETWORK_Handle> -> Ptr () -> CSize -> IO CLong
#ccall GNUNET_NETWORK_socket_select , Ptr <struct GNUNET_NETWORK_FDSet> -> Ptr <struct GNUNET_NETWORK_FDSet> -> Ptr <struct GNUNET_NETWORK_FDSet> -> <GNUNET_TIME_Relative> -> IO CInt
#ccall GNUNET_NETWORK_socket_send , Ptr <struct GNUNET_NETWORK_Handle> -> Ptr () -> CSize -> IO CLong
#ccall GNUNET_NETWORK_socket_sendto , Ptr <struct GNUNET_NETWORK_Handle> -> Ptr () -> CSize -> Ptr <struct sockaddr> -> CUInt -> IO CLong
#ccall GNUNET_NETWORK_socket_setsockopt , Ptr <struct GNUNET_NETWORK_Handle> -> CInt -> CInt -> Ptr () -> CUInt -> IO CInt
#ccall GNUNET_NETWORK_socket_shutdown , Ptr <struct GNUNET_NETWORK_Handle> -> CInt -> IO CInt
#ccall GNUNET_NETWORK_socket_disable_corking , Ptr <struct GNUNET_NETWORK_Handle> -> IO CInt
#ccall GNUNET_NETWORK_socket_create , CInt -> CInt -> CInt -> IO (Ptr <struct GNUNET_NETWORK_Handle>)
#ccall GNUNET_NETWORK_fdset_zero , Ptr <struct GNUNET_NETWORK_FDSet> -> IO ()
#ccall GNUNET_NETWORK_fdset_set , Ptr <struct GNUNET_NETWORK_FDSet> -> Ptr <struct GNUNET_NETWORK_Handle> -> IO ()
#ccall GNUNET_NETWORK_fdset_isset , Ptr <struct GNUNET_NETWORK_FDSet> -> Ptr <struct GNUNET_NETWORK_Handle> -> IO CInt
#ccall GNUNET_NETWORK_fdset_add , Ptr <struct GNUNET_NETWORK_FDSet> -> Ptr <struct GNUNET_NETWORK_FDSet> -> IO ()
#ccall GNUNET_NETWORK_fdset_copy , Ptr <struct GNUNET_NETWORK_FDSet> -> Ptr <struct GNUNET_NETWORK_FDSet> -> IO ()
#ccall GNUNET_NETWORK_get_fd , Ptr <struct GNUNET_NETWORK_Handle> -> IO CInt
#ccall GNUNET_NETWORK_get_addr , Ptr <struct GNUNET_NETWORK_Handle> -> IO (Ptr <struct sockaddr>)
#ccall GNUNET_NETWORK_get_addrlen , Ptr <struct GNUNET_NETWORK_Handle> -> IO CUInt
#ccall GNUNET_NETWORK_fdset_copy_native , Ptr <struct GNUNET_NETWORK_FDSet> -> Ptr <fd_set> -> CInt -> IO ()
#ccall GNUNET_NETWORK_fdset_set_native , Ptr <struct GNUNET_NETWORK_FDSet> -> CInt -> IO ()
#ccall GNUNET_NETWORK_fdset_test_native , Ptr <struct GNUNET_NETWORK_FDSet> -> CInt -> IO CInt
#ccall GNUNET_NETWORK_fdset_handle_set , Ptr <struct GNUNET_NETWORK_FDSet> -> Ptr <struct GNUNET_DISK_FileHandle> -> IO ()
#ccall GNUNET_NETWORK_fdset_handle_set_first , Ptr <struct GNUNET_NETWORK_FDSet> -> Ptr <struct GNUNET_DISK_FileHandle> -> IO ()
#ccall GNUNET_NETWORK_fdset_handle_isset , Ptr <struct GNUNET_NETWORK_FDSet> -> Ptr <struct GNUNET_DISK_FileHandle> -> IO CInt
#ccall GNUNET_NETWORK_fdset_overlap , Ptr <struct GNUNET_NETWORK_FDSet> -> Ptr <struct GNUNET_NETWORK_FDSet> -> IO CInt
#ccall GNUNET_NETWORK_fdset_create , IO (Ptr <struct GNUNET_NETWORK_FDSet>)
#ccall GNUNET_NETWORK_fdset_destroy , Ptr <struct GNUNET_NETWORK_FDSet> -> IO ()
#ccall GNUNET_NETWORK_test_port_free , CInt -> CUShort -> IO CInt
