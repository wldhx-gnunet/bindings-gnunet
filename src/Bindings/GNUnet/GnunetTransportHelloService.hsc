{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_transport_hello_service.h"
module Bindings.GNUnet.GnunetTransportHelloService where
import Foreign.Ptr
#strict_import

import Bindings.GNUnet.GnunetCommon (C'GNUNET_MessageHeader)
import Bindings.GNUnet.GnunetConfigurationLib (C'GNUNET_CONFIGURATION_Handle)
import Bindings.GNUnet.GnunetCryptoTypes (C'GNUNET_PeerIdentity)
import Bindings.GNUnet.GnunetTimeLib (C'GNUNET_TIME_Absolute)
import Bindings.GNUnet.GnunetNtLib (C'GNUNET_NetworkType)

{- enum GNUNET_TRANSPORT_AddressClass {
    GNUNET_TRANSPORT_AC_NONE = 0,
    GNUNET_TRANSPORT_AC_OTHER = 1,
    GNUNET_TRANSPORT_AC_GLOBAL = 2,
    GNUNET_TRANSPORT_AC_GLOBAL_PRIVATE = 4,
    GNUNET_TRANSPORT_AC_LAN = 8,
    GNUNET_TRANSPORT_AC_WLAN = 16,
    GNUNET_TRANSPORT_AC_BT = 32,
    GNUNET_TRANSPORT_AC_ANY = 65535
}; -}
#integral_t enum GNUNET_TRANSPORT_AddressClass
#num GNUNET_TRANSPORT_AC_NONE
#num GNUNET_TRANSPORT_AC_OTHER
#num GNUNET_TRANSPORT_AC_GLOBAL
#num GNUNET_TRANSPORT_AC_GLOBAL_PRIVATE
#num GNUNET_TRANSPORT_AC_LAN
#num GNUNET_TRANSPORT_AC_WLAN
#num GNUNET_TRANSPORT_AC_BT
#num GNUNET_TRANSPORT_AC_ANY
#callback GNUNET_TRANSPORT_HelloUpdateCallback , Ptr () -> Ptr <struct GNUNET_MessageHeader> -> IO ()
{- struct GNUNET_TRANSPORT_HelloGetHandle; -}
#opaque_t struct GNUNET_TRANSPORT_HelloGetHandle
#ccall GNUNET_TRANSPORT_hello_get , Ptr <struct GNUNET_CONFIGURATION_Handle> -> <enum GNUNET_TRANSPORT_AddressClass> -> <GNUNET_TRANSPORT_HelloUpdateCallback> -> Ptr () -> IO (Ptr <struct GNUNET_TRANSPORT_HelloGetHandle>)
#ccall GNUNET_TRANSPORT_hello_get_cancel , Ptr <struct GNUNET_TRANSPORT_HelloGetHandle> -> IO ()
#callback GNUNET_TRANSPORT_AddressCallback , Ptr () -> Ptr <struct GNUNET_PeerIdentity> -> CString -> <enum GNUNET_NetworkType> -> <struct GNUNET_TIME_Absolute> -> IO ()
