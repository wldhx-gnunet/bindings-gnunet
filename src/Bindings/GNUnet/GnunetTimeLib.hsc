{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_time_lib.h"
module Bindings.GNUnet.GnunetTimeLib where
import Foreign.Ptr
#strict_import

-- Type aliases used here instead of normal struct wrappers due to toolchain not supporting calling foreign funcs with struct passed by value
-- See https://github.com/jwiegley/bindings-dsl/issues/1
-- Fortunately, we can get away with this since these are single-element structs
type C'GNUNET_TIME_Absolute = CULong
type C'GNUNET_TIME_Relative = CULong
type C'GNUNET_TIME_RelativeNBO = CULong
type C'GNUNET_TIME_AbsoluteNBO = CULong
#ccall GNUNET_TIME_relative_get_zero_ , (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_absolute_get_zero_ , (<GNUNET_TIME_Absolute>)
#ccall GNUNET_TIME_relative_get_unit_ , (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_relative_get_millisecond_ , (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_relative_get_second_ , (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_relative_get_minute_ , (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_relative_get_hour_ , (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_relative_get_forever_ , (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_absolute_get_forever_ , (<GNUNET_TIME_Absolute>)
#ccall GNUNET_TIME_absolute_get , IO (<GNUNET_TIME_Absolute>)
#ccall GNUNET_TIME_relative_to_absolute , <GNUNET_TIME_Relative> -> IO (<GNUNET_TIME_Absolute>)
#ccall GNUNET_TIME_round_abs , Ptr <GNUNET_TIME_Absolute> -> IO CInt
#ccall GNUNET_TIME_round_rel , Ptr <GNUNET_TIME_Relative> -> IO CInt
#ccall GNUNET_TIME_relative_min , <GNUNET_TIME_Relative> -> <GNUNET_TIME_Relative> -> (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_relative_max , <GNUNET_TIME_Relative> -> <GNUNET_TIME_Relative> -> (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_absolute_min , <GNUNET_TIME_Absolute> -> <GNUNET_TIME_Absolute> -> (<GNUNET_TIME_Absolute>)
#ccall GNUNET_TIME_absolute_max , <GNUNET_TIME_Absolute> -> <GNUNET_TIME_Absolute> -> (<GNUNET_TIME_Absolute>)
#ccall GNUNET_TIME_absolute_get_remaining , <GNUNET_TIME_Absolute> -> IO (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_calculate_eta , <GNUNET_TIME_Absolute> -> CULong -> CULong -> IO (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_absolute_get_difference , <GNUNET_TIME_Absolute> -> <GNUNET_TIME_Absolute> -> IO (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_absolute_get_duration , <GNUNET_TIME_Absolute> -> IO (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_absolute_add , <GNUNET_TIME_Absolute> -> <GNUNET_TIME_Relative> -> IO (<GNUNET_TIME_Absolute>)
#ccall GNUNET_TIME_absolute_subtract , <GNUNET_TIME_Absolute> -> <GNUNET_TIME_Relative> -> IO (<GNUNET_TIME_Absolute>)
#ccall GNUNET_TIME_relative_multiply , <GNUNET_TIME_Relative> -> CULong -> IO (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_relative_saturating_multiply , <GNUNET_TIME_Relative> -> CULong -> IO (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_relative_divide , <GNUNET_TIME_Relative> -> CULong -> IO (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_relative_add , <GNUNET_TIME_Relative> -> <GNUNET_TIME_Relative> -> IO (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_relative_subtract , <GNUNET_TIME_Relative> -> <GNUNET_TIME_Relative> -> IO (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_relative_hton , <GNUNET_TIME_Relative> -> IO (<GNUNET_TIME_RelativeNBO>)
#ccall GNUNET_TIME_relative_ntoh , <GNUNET_TIME_RelativeNBO> -> IO (<GNUNET_TIME_Relative>)
#ccall GNUNET_TIME_absolute_hton , <GNUNET_TIME_Absolute> -> IO (<GNUNET_TIME_AbsoluteNBO>)
#ccall GNUNET_TIME_absolute_ntoh , <GNUNET_TIME_AbsoluteNBO> -> IO (<GNUNET_TIME_Absolute>)
#ccall GNUNET_TIME_set_offset , CLong -> IO ()
#ccall GNUNET_TIME_get_offset , IO CLong
#ccall GNUNET_TIME_get_current_year , IO CUInt
#ccall GNUNET_TIME_year_to_time , CUInt -> IO (<GNUNET_TIME_Absolute>)
#ccall GNUNET_TIME_time_to_year , <GNUNET_TIME_Absolute> -> IO CUInt
