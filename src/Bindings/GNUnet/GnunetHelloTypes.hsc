{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_hello_lib.h"
module Bindings.GNUnet.GnunetHelloTypes where
import Foreign.Ptr
#strict_import

import Bindings.GNUnet.GnunetCommon
import Bindings.GNUnet.GnunetCryptoTypes (C'GNUNET_PeerIdentity, C'GNUNET_CRYPTO_EddsaPublicKey)

{- enum GNUNET_HELLO_AddressInfo {
    GNUNET_HELLO_ADDRESS_INFO_NONE = 0,
    GNUNET_HELLO_ADDRESS_INFO_INBOUND = 1
}; -}
#integral_t enum GNUNET_HELLO_AddressInfo
#num GNUNET_HELLO_ADDRESS_INFO_NONE
#num GNUNET_HELLO_ADDRESS_INFO_INBOUND
{- struct GNUNET_HELLO_Address {
    struct GNUNET_PeerIdentity peer;
    const char * transport_name;
    const void * address;
    size_t address_length;
    enum GNUNET_HELLO_AddressInfo local_info;
}; -}
#starttype struct GNUNET_HELLO_Address
#field peer , <struct GNUNET_PeerIdentity>
#field transport_name , CString
#field address , Ptr ()
#field address_length , CSize
#field local_info , <enum GNUNET_HELLO_AddressInfo>
#stoptype
{- struct GNUNET_HELLO_Message {
    struct GNUNET_MessageHeader header;
    uint32_t friend_only __attribute__((packed));
    struct GNUNET_CRYPTO_EddsaPublicKey publicKey;
}; -}
#starttype struct GNUNET_HELLO_Message
#field header , <struct GNUNET_MessageHeader>
#field friend_only , CUInt
#field publicKey , <struct GNUNET_CRYPTO_EddsaPublicKey>
#stoptype
