{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_statistics_service.h"
module Bindings.GNUnet.GnunetStatisticsService where
import Foreign.Ptr
#strict_import

import Bindings.GNUnet.GnunetConfigurationLib (C'GNUNET_CONFIGURATION_Handle)

{- struct GNUNET_STATISTICS_Handle; -}
#opaque_t struct GNUNET_STATISTICS_Handle
#callback GNUNET_STATISTICS_Iterator , Ptr () -> CString -> CString -> CULong -> CInt -> IO CInt
#ccall GNUNET_STATISTICS_create , CString -> Ptr <struct GNUNET_CONFIGURATION_Handle> -> IO (Ptr <struct GNUNET_STATISTICS_Handle>)
#ccall GNUNET_STATISTICS_destroy , Ptr <struct GNUNET_STATISTICS_Handle> -> CInt -> IO ()
#ccall GNUNET_STATISTICS_watch , Ptr <struct GNUNET_STATISTICS_Handle> -> CString -> CString -> <GNUNET_STATISTICS_Iterator> -> Ptr () -> IO CInt
#ccall GNUNET_STATISTICS_watch_cancel , Ptr <struct GNUNET_STATISTICS_Handle> -> CString -> CString -> <GNUNET_STATISTICS_Iterator> -> Ptr () -> IO CInt
#callback GNUNET_STATISTICS_Callback , Ptr () -> CInt -> IO ()
{- struct GNUNET_STATISTICS_GetHandle; -}
#opaque_t struct GNUNET_STATISTICS_GetHandle
#ccall GNUNET_STATISTICS_get , Ptr <struct GNUNET_STATISTICS_Handle> -> CString -> CString -> <GNUNET_STATISTICS_Callback> -> <GNUNET_STATISTICS_Iterator> -> Ptr () -> IO (Ptr <struct GNUNET_STATISTICS_GetHandle>)
#ccall GNUNET_STATISTICS_get_cancel , Ptr <struct GNUNET_STATISTICS_GetHandle> -> IO ()
#ccall GNUNET_STATISTICS_set , Ptr <struct GNUNET_STATISTICS_Handle> -> CString -> CULong -> CInt -> IO ()
#ccall GNUNET_STATISTICS_update , Ptr <struct GNUNET_STATISTICS_Handle> -> CString -> CLong -> CInt -> IO ()
