{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_transport_plugin.h"
module Bindings.GNUnet.GnunetTransportPlugin where
import Foreign.Ptr
#strict_import

import Bindings.GNUnet.GnunetSchedulerLib
import Bindings.GNUnet.GnunetStatisticsService
import Bindings.GNUnet.GnunetTransportService
import Bindings.GNUnet.GnunetAtsService
import Bindings.GNUnet.GnunetNtLib (C'GNUNET_NetworkType)

import Bindings.GNUnet.GnunetCommon
import Bindings.GNUnet.GnunetConfigurationLib
import Bindings.GNUnet.GnunetCryptoTypes (C'GNUNET_PeerIdentity)
import Bindings.GNUnet.GnunetTransportService
import Bindings.GNUnet.GnunetTimeLib (C'GNUNET_TIME_Absolute, C'GNUNET_TIME_Relative)
import Bindings.GNUnet.GnunetHelloTypes

#callback GNUNET_TRANSPORT_SessionEnd , Ptr () -> Ptr <struct GNUNET_HELLO_Address> -> Ptr <struct GNUNET_ATS_Session> -> IO ()
#callback GNUNET_TRANSPORT_SessionStart , Ptr () -> Ptr <struct GNUNET_HELLO_Address> -> Ptr <struct GNUNET_ATS_Session> -> <enum GNUNET_NetworkType> -> IO ()
#callback GNUNET_TRANSPORT_PluginReceiveCallback , Ptr () -> Ptr <struct GNUNET_HELLO_Address> -> Ptr <struct GNUNET_ATS_Session> -> Ptr <struct GNUNET_MessageHeader> -> IO (<struct GNUNET_TIME_Relative>)
#callback GNUNET_TRANSPORT_AddressToType , Ptr () -> Ptr <struct sockaddr> -> CSize -> IO (<enum GNUNET_NetworkType>)
#callback GNUNET_TRANSPORT_UpdateAddressDistance , Ptr () -> Ptr <struct GNUNET_HELLO_Address> -> CUInt -> IO ()
#callback GNUNET_TRANSPORT_AddressNotification , Ptr () -> CInt -> Ptr <struct GNUNET_HELLO_Address> -> IO ()
#callback GNUNET_TRANSPORT_TrafficReport , Ptr () -> Ptr <struct GNUNET_PeerIdentity> -> CSize -> IO (<struct GNUNET_TIME_Relative>)
#callback GNUNET_TRANSPORT_GetHelloCallback , IO (Ptr <struct GNUNET_MessageHeader>)
{- struct GNUNET_TRANSPORT_PluginEnvironment {
    const struct GNUNET_CONFIGURATION_Handle * cfg;
    const struct GNUNET_PeerIdentity * my_identity;
    void * cls;
    struct GNUNET_STATISTICS_Handle * stats;
    GNUNET_TRANSPORT_PluginReceiveCallback receive;
    GNUNET_TRANSPORT_GetHelloCallback get_our_hello;
    GNUNET_TRANSPORT_AddressNotification notify_address;
    GNUNET_TRANSPORT_SessionEnd session_end;
    GNUNET_TRANSPORT_SessionStart session_start;
    GNUNET_TRANSPORT_AddressToType get_address_type;
    GNUNET_TRANSPORT_UpdateAddressDistance update_address_distance;
    uint32_t max_connections;
}; -}
#starttype struct GNUNET_TRANSPORT_PluginEnvironment
#field cfg , Ptr <struct GNUNET_CONFIGURATION_Handle>
#field my_identity , Ptr <struct GNUNET_PeerIdentity>
#field cls , Ptr ()
#field stats , Ptr <struct GNUNET_STATISTICS_Handle>
#field receive , <GNUNET_TRANSPORT_PluginReceiveCallback>
#field get_our_hello , <GNUNET_TRANSPORT_GetHelloCallback>
#field notify_address , <GNUNET_TRANSPORT_AddressNotification>
#field session_end , <GNUNET_TRANSPORT_SessionEnd>
#field session_start , <GNUNET_TRANSPORT_SessionStart>
#field get_address_type , <GNUNET_TRANSPORT_AddressToType>
#field update_address_distance , <GNUNET_TRANSPORT_UpdateAddressDistance>
#field max_connections , CUInt
#stoptype
#callback GNUNET_TRANSPORT_TransmitContinuation , Ptr () -> Ptr <struct GNUNET_PeerIdentity> -> CInt -> CSize -> CSize -> IO ()
#callback GNUNET_TRANSPORT_TransmitFunction , Ptr () -> Ptr <struct GNUNET_ATS_Session> -> CString -> CSize -> CUInt -> <struct GNUNET_TIME_Relative> -> <GNUNET_TRANSPORT_TransmitContinuation> -> Ptr () -> IO CLong
#callback GNUNET_TRANSPORT_DisconnectSessionFunction , Ptr () -> Ptr <struct GNUNET_ATS_Session> -> IO CInt
#callback GNUNET_TRANSPORT_QueryKeepaliveFactorFunction , Ptr () -> IO CUInt
#callback GNUNET_TRANSPORT_DisconnectPeerFunction , Ptr () -> Ptr <struct GNUNET_PeerIdentity> -> IO ()
#callback GNUNET_TRANSPORT_AddressStringCallback , Ptr () -> CString -> CInt -> IO ()
#callback GNUNET_TRANSPORT_AddressPrettyPrinter , Ptr () -> CString -> Ptr () -> CSize -> CInt -> <struct GNUNET_TIME_Relative> -> <GNUNET_TRANSPORT_AddressStringCallback> -> Ptr () -> IO ()
#callback GNUNET_TRANSPORT_CheckAddress , Ptr () -> Ptr () -> CSize -> IO CInt
#callback GNUNET_TRANSPORT_CreateSession , Ptr () -> Ptr <struct GNUNET_HELLO_Address> -> IO (Ptr <struct GNUNET_ATS_Session>)
#callback GNUNET_TRANSPORT_UpdateSessionTimeout , Ptr () -> Ptr <struct GNUNET_PeerIdentity> -> Ptr <struct GNUNET_ATS_Session> -> IO ()
#callback GNUNET_TRANSPORT_UpdateInboundDelay , Ptr () -> Ptr <struct GNUNET_PeerIdentity> -> Ptr <struct GNUNET_ATS_Session> -> <struct GNUNET_TIME_Relative> -> IO ()
#callback GNUNET_TRANSPORT_AddressToString , Ptr () -> Ptr () -> CSize -> IO CString
#callback GNUNET_TRANSPORT_StringToAddress , Ptr () -> CString -> CUShort -> Ptr (Ptr ()) -> Ptr CSize -> IO CInt
#callback GNUNET_TRANSPORT_GetNetworkType , Ptr () -> Ptr <struct GNUNET_ATS_Session> -> IO (<enum GNUNET_NetworkType>)
#callback GNUNET_TRANSPORT_GetNetworkTypeForAddress , Ptr () -> Ptr <struct GNUNET_HELLO_Address> -> IO (<enum GNUNET_NetworkType>)
#callback GNUNET_TRANSPORT_SessionInfoCallback , Ptr () -> Ptr <struct GNUNET_ATS_Session> -> Ptr <struct GNUNET_TRANSPORT_SessionInfo> -> IO ()
#callback GNUNET_TRANSPORT_SessionMonitorSetup , Ptr () -> <GNUNET_TRANSPORT_SessionInfoCallback> -> Ptr () -> IO ()
{- struct GNUNET_TRANSPORT_PluginFunctions {
    void * cls;
    GNUNET_TRANSPORT_TransmitFunction send;
    GNUNET_TRANSPORT_DisconnectPeerFunction disconnect_peer;
    GNUNET_TRANSPORT_DisconnectSessionFunction disconnect_session;
    GNUNET_TRANSPORT_UpdateSessionTimeout update_session_timeout;
    GNUNET_TRANSPORT_UpdateInboundDelay update_inbound_delay;
    GNUNET_TRANSPORT_QueryKeepaliveFactorFunction query_keepalive_factor;
    GNUNET_TRANSPORT_AddressPrettyPrinter address_pretty_printer;
    GNUNET_TRANSPORT_CheckAddress check_address;
    GNUNET_TRANSPORT_AddressToString address_to_string;
    GNUNET_TRANSPORT_StringToAddress string_to_address;
    GNUNET_TRANSPORT_CreateSession get_session;
    GNUNET_TRANSPORT_GetNetworkType get_network;
    GNUNET_TRANSPORT_GetNetworkTypeForAddress get_network_for_address;
    GNUNET_TRANSPORT_SessionMonitorSetup setup_monitor;
}; -}
#starttype struct GNUNET_TRANSPORT_PluginFunctions
#field cls , Ptr ()
#field send , <GNUNET_TRANSPORT_TransmitFunction>
#field disconnect_peer , <GNUNET_TRANSPORT_DisconnectPeerFunction>
#field disconnect_session , <GNUNET_TRANSPORT_DisconnectSessionFunction>
#field update_session_timeout , <GNUNET_TRANSPORT_UpdateSessionTimeout>
#field update_inbound_delay , <GNUNET_TRANSPORT_UpdateInboundDelay>
#field query_keepalive_factor , <GNUNET_TRANSPORT_QueryKeepaliveFactorFunction>
#field address_pretty_printer , <GNUNET_TRANSPORT_AddressPrettyPrinter>
#field check_address , <GNUNET_TRANSPORT_CheckAddress>
#field address_to_string , <GNUNET_TRANSPORT_AddressToString>
#field string_to_address , <GNUNET_TRANSPORT_StringToAddress>
#field get_session , <GNUNET_TRANSPORT_CreateSession>
#field get_network , <GNUNET_TRANSPORT_GetNetworkType>
#field get_network_for_address , <GNUNET_TRANSPORT_GetNetworkTypeForAddress>
#field setup_monitor , <GNUNET_TRANSPORT_SessionMonitorSetup>
#stoptype
