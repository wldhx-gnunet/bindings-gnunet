{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_nt_lib.h"
module Bindings.GNUnet.GnunetNtLib where
import Foreign.Ptr
#strict_import

{- enum GNUNET_NetworkType {
    GNUNET_NT_UNSPECIFIED = 0,
    GNUNET_NT_LOOPBACK = 1,
    GNUNET_NT_LAN = 2,
    GNUNET_NT_WAN = 3,
    GNUNET_NT_WLAN = 4,
    GNUNET_NT_BT = 5
}; -}
#integral_t enum GNUNET_NetworkType
#num GNUNET_NT_UNSPECIFIED
#num GNUNET_NT_LOOPBACK
#num GNUNET_NT_LAN
#num GNUNET_NT_WAN
#num GNUNET_NT_WLAN
#num GNUNET_NT_BT
{- struct GNUNET_NT_InterfaceScanner; -}
#opaque_t struct GNUNET_NT_InterfaceScanner
