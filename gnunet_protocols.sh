# replace #define'd constants with #num_patterns, strip out numbers they are defined too, clean up trailing whitespace, and skip first line (which is a header guard)
grep '^#define' ~/Projects/gnunet/gnunet/src/include/gnunet_protocols.h | sed s/'^#define '/'#num_pattern '/ | sed -E s/' ([0-9]+)$'//g | sed s/' *$'//g | tail -n +2
