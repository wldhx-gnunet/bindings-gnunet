module EcdsaKeySpec (spec) where

import Test.Hspec
import Foreign.Storable
import Bindings.GNUnet.GnunetCryptoLib

spec :: Spec
spec = describe "ecdsa_key_create" $ do
    -- FIXME
    ((x, a), (y, b)) <- runIO $ do
        x <- c'GNUNET_CRYPTO_ecdsa_key_create
        y <- c'GNUNET_CRYPTO_ecdsa_key_create

        a <- peek x
        b <- peek y

        return ((x, a), (y, b))

    it "p'(keygen()) /= p'(keygen())" $ x `shouldNotBe` y
    it "keygen() /= keygen()" $ a `shouldNotBe` b
