{ pkgs ? import <nixpkgs> { overlays = [ (import <gnunet-nix/overlay.nix>) (import <gnunet-nix/haskell-overrides.nix>) ]; } }:
{
  bindings-gnunet = import ./default.nix { inherit pkgs; };
}
